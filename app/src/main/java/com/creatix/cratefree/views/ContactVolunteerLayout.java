package com.creatix.cratefree.views;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.views.lists.adapters.FormLayout;

import java.util.HashMap;


public class ContactVolunteerLayout extends LinearLayout implements FormLayout {

    private EditText emailText;
    private EditText phoneText;
    private EditText cityText;
    private EditText stateText;
    private EditText firstNameText;
    private EditText lastNameText;
    private EditText commentText;

    private SwitchCompat adultSwitch;
    private SwitchCompat prevExperienceSwitch;

    private String email;
    private String phone;
    private String city;
    private String state;
    private String firstName;
    private String lastName;
    private String comment;

    public ContactVolunteerLayout(Context context) {
        super(context);
        inflate(context, R.layout.layout_volunteer, this);

        emailText = (EditText) findViewById(R.id.edittext_email);
        phoneText = (EditText) findViewById(R.id.edittext_phone);
        cityText = (EditText) findViewById(R.id.edittext_city);
        stateText = (EditText) findViewById(R.id.edittext_state);
        firstNameText = (EditText) findViewById(R.id.edittext_first_name);
        lastNameText = (EditText) findViewById(R.id.edittext_last_name);
        commentText = (EditText) findViewById(R.id.edittext_comments);

        adultSwitch = (SwitchCompat) findViewById(R.id.swith_adult);
        prevExperienceSwitch = (SwitchCompat) findViewById(R.id.switch_experience);
    }

    @Override
    public HashMap<String, Object> getParams() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("phone", phone);
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("text", comment);
        params.put("city", city);
        params.put("state", state);
        params.put("over18", adultSwitch.isChecked());
        params.put("previousExperience", prevExperienceSwitch.isChecked());

        return params;
    }

    @Override
    public boolean isValid() {

        email = emailText.getText().toString();
        phone = phoneText.getText().toString();
        city = cityText.getText().toString();
        state = stateText.getText().toString();
        firstName = firstNameText.getText().toString();
        lastName = lastNameText.getText().toString();
        comment = commentText.getText().toString();

        if (!Utils.isEmailValid(email)) {
            Utils.showToast(getContext(), getResources().getString(R.string.email_invalid));
            return false;
        } else if (!TextUtils.isEmpty(phone) && !PhoneNumberUtils.isGlobalPhoneNumber(phone)) {
            Utils.showToast(getContext(), getResources().getString(R.string.farm_name_invalid));
            return false;
        } else if (city == null || city.length() < 2) {
            Utils.showToast(getContext(), getResources().getString(R.string.city_invalid));
            return false;
        } else if (state == null || state.length() < 2) {
            Utils.showToast(getContext(), getResources().getString(R.string.state_invalid));
            return false;
        } else if (firstName == null || firstName.length() < 2) {
            Utils.showToast(getContext(), getResources().getString(R.string.first_name_invalid));
            return false;
        } else if (lastName == null || lastName.length() < 2) {
            Utils.showToast(getContext(), getResources().getString(R.string.last_name_invalid));
            return false;
        }
        return true;
    }
}
