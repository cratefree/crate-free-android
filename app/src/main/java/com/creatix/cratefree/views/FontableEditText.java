package com.creatix.cratefree.views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.UiUtil;

public class FontableEditText extends AppCompatEditText {

    AttributeSet mAttrs;

    public FontableEditText(Context context) {
        super(context);
    }

    public FontableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttrs(attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_creatix_cratefree_views_FontableEditText,
                R.styleable.com_creatix_cratefree_views_FontableEditText_fontName);
    }

    public FontableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setAttrs(attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_creatix_cratefree_views_FontableEditText,
                R.styleable.com_creatix_cratefree_views_FontableEditText_fontName);
    }

    public AttributeSet getAttrs() {
        return mAttrs;
    }

    public void setAttrs(AttributeSet mAttrs) {
        this.mAttrs = mAttrs;
    }
}