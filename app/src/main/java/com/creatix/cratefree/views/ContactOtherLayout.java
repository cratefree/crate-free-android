package com.creatix.cratefree.views;

import android.content.Context;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.views.lists.adapters.FormLayout;

import java.util.HashMap;


public class ContactOtherLayout extends LinearLayout implements FormLayout{

    private EditText emailText;
    private EditText phoneText;
    private EditText firstNameText;
    private EditText lastNameText;
    private EditText commentText;

    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private String comment;


    public ContactOtherLayout(Context context) {
        super(context);
        inflate(context, R.layout.layout_other, this);

        emailText = (EditText) findViewById(R.id.edittext_email);
        phoneText = (EditText) findViewById(R.id.edittext_phone);
        firstNameText = (EditText) findViewById(R.id.edittext_first_name);
        lastNameText = (EditText) findViewById(R.id.edittext_last_name);
        commentText = (EditText) findViewById(R.id.edittext_comments);
    }

    @Override
    public HashMap<String, Object> getParams() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("phone", phone);
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("", comment);

        return params;
    }

    @Override
    public boolean isValid() {

        email = emailText.getText().toString();
        phone = phoneText.getText().toString();
        firstName = firstNameText.getText().toString();
        lastName = lastNameText.getText().toString();
        comment = commentText.getText().toString();

        if (!Utils.isEmailValid(email)) {
            Utils.showToast(getContext(), getResources().getString(R.string.email_invalid));
            return false;
        } else if (!TextUtils.isEmpty(phone) && !PhoneNumberUtils.isGlobalPhoneNumber(phone)) {
            Utils.showToast(getContext(), getResources().getString(R.string.farm_name_invalid));
            return false;
        } else if (firstName == null || firstName.length() < 2) {
            Utils.showToast(getContext(), getResources().getString(R.string.first_name_invalid));
            return false;
        } else if (lastName == null || lastName.length() < 2) {
            Utils.showToast(getContext(), getResources().getString(R.string.last_name_invalid));
            return false;
        }

        return true;
    }
}
