package com.creatix.cratefree.views.lists.adapters.items;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.creatix.cratefree.utils.listeners.OnItemClickListener;

/**
 * Description: FIXME
 *
 * @author <a href="mailto:"></a>
 * @version 02/08/2017
 */

public abstract class CustomHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected View itemView;

    protected Context context;
    protected int numberOfItemsAbove;
    protected OnItemClickListener mListener;

    public CustomHolder(View itemView,
                        Context context) {
        super(itemView);

        this.itemView = itemView;
        this.context = context;
    }

    public CustomHolder(View itemView,
                        Context context,
                        int numberOfItemsAbove,
                        OnItemClickListener mListener) {
        super(itemView);

        this.itemView = itemView;
        this.context = context;
        this.numberOfItemsAbove = numberOfItemsAbove;
        this.mListener = mListener;
    }

    @Override
    public abstract void onClick(View v);
}
