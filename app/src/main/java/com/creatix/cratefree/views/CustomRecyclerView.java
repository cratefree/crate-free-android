package com.creatix.cratefree.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnScrolledListener;

/**
 * Created by jano on 3.12.2015.
 */
public class CustomRecyclerView extends RecyclerView {

    private OnScrolledListener onScrolledListener;

    public void setOnScrolledListener(OnScrolledListener onScrolledListener) {
        this.onScrolledListener = onScrolledListener;
    }

    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        addItemDecoration(new DividerItemDecoration(context));
        setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        setLayoutManager(mLayoutManager);
        setItemAnimator(new DefaultItemAnimator());
    }

    private int scrolled = 0;

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        if (onScrolledListener != null) {
            onScrolledListener.onScrolled(scrolled += dy);
        }
    }

    public class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable mDivider;

        private Context context;

        private static final int PADDING = 10;

        /**
         * Default divider will be used
         */
        public DividerItemDecoration(Context context) {
            this.context = context;
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            mDivider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }

        /**
         * Custom divider will be used
         */
        public DividerItemDecoration(Context context, int resId) {
            this.context = context;
            mDivider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = Utils.getPixels(context, PADDING);
            int right = parent.getWidth() - Utils.getPixels(context, PADDING);

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }
}
