package com.creatix.cratefree.views.lists.adapters.items;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.creatix.cratefree.R;
import com.creatix.cratefree.api.objects.TypeInterface;
import com.creatix.cratefree.utils.listeners.OnItemClickListener;

import java.util.List;

public class FilterHolder<T extends TypeInterface> extends CustomHolder {

    private TextView titleTextView;
    private ImageView imageProduct;
    private ImageView selectedImage;

    private List<T> filters;
    private List<T> selectedFilters;

    private View divider;

    public FilterHolder(View itemView,
                        Context context,
                        int numberOfItemsAbove,
                        OnItemClickListener mListener,
                        List<T> filters,
                        List<T> selectedFilters) {
        super(itemView, context, numberOfItemsAbove, mListener);

        this.filters = filters;
        this.selectedFilters = selectedFilters;

        titleTextView = (TextView) itemView.findViewById(R.id.title_product);
        imageProduct = (ImageView) itemView.findViewById(R.id.image_product);
        selectedImage = (ImageView) itemView.findViewById(R.id.image_selected);

        divider = itemView.findViewById(R.id.divider);

        itemView.setOnClickListener(this);
    }

    public void setView(int position) {
        T t = filters.get(position);
        titleTextView.setText(t.getLabel());
        setSelected(t);
        imageProduct.setImageResource(t.getRes());

        divider.setVisibility(position == filters.size() - 1 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        T t = filters.get(getAdapterPosition() - numberOfItemsAbove);
        mListener.onItemClick(t);
        setSelected(t);
    }

    private void setSelected(T t) {
        selectedImage.setVisibility(selectedFilters.contains(t) ? View.VISIBLE : View.INVISIBLE);
        imageProduct.setAlpha(selectedFilters.contains(t) ? 1f : 0.4f);
    }

    public void unselect() {
        selectedFilters.clear();
        selectedImage.setVisibility(View.INVISIBLE);
        imageProduct.setAlpha(0.4f);
    }
}
