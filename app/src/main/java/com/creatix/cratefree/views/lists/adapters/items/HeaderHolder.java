package com.creatix.cratefree.views.lists.adapters.items;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.creatix.cratefree.R;

/**
 * Description: FIXME
 *
 * @author <a href="mailto:"></a>
 * @version 02/08/2017
 */

public class HeaderHolder extends CustomHolder {

    public HeaderHolder(View itemView, Context context, int resId) {
        super(itemView, context);

        TextView label = (TextView) itemView.findViewById(R.id.section_title);
        label.setText(context.getString(resId));
    }

    @Override
    public void onClick(View v) {
        // NOT USED
    }
}
