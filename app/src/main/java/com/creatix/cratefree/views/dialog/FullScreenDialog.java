package com.creatix.cratefree.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;


/**
 * Created by jjancek on 2/25/15.
 */
public class FullScreenDialog extends Dialog {

    public FullScreenDialog(Context context, int layout, int style) {
        super(context, style);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCanceledOnTouchOutside(true);

        setContentView(layout);
    }
}
