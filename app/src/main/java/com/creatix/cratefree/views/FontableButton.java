package com.creatix.cratefree.views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.UiUtil;

public class FontableButton extends AppCompatButton {

    public FontableButton(Context context) {
        super(context);
    }

    public FontableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_creatix_cratefree_views_FontableButton,
                R.styleable.com_creatix_cratefree_views_FontableButton_fontName);
    }

    public FontableButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_creatix_cratefree_views_FontableButton,
                R.styleable.com_creatix_cratefree_views_FontableButton_fontName);
    }
}