package com.creatix.cratefree.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.listeners.TouchActionListener;


public class ContactLayout extends LinearLayout {

    private TextView labelView;
    private FontableTextView titleView;

    public ContactLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContactLayout(Context context) {
        super(context);
        inflate(context, R.layout.layout_contact, this);

        labelView = (TextView) findViewById(R.id.label);
        titleView = (FontableTextView) findViewById(R.id.title);
    }

    public void setViews(String label, String title) {
        labelView.setText(label);
        titleView.setText(title);
    }

    public void setOnClickListener(TouchActionListener touchActionListener) {
        titleView.setTextColor(getResources().getColor(R.color.blue_lt));
        titleView.registerClickableTouchListener(touchActionListener);
    }
}
