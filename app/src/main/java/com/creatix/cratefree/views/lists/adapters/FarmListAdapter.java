package com.creatix.cratefree.views.lists.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creatix.cratefree.R;
import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.api.objects.Product;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnItemClickListener;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FarmListAdapter extends
        RecyclerView.Adapter<FarmListAdapter.ViewHolder> implements Filterable {

    private List<Farm> mFilteredFarms;
    private List<Farm> mAllFarms;

    protected OnItemClickListener mListener;

    public FarmListAdapter(List<Farm> farms) {
        mFilteredFarms = farms;
        mAllFarms = farms;
    }

    public void setFarms(List<Farm> farms) {
        mFilteredFarms = farms;
        mAllFarms = farms;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.layout_farm_list_item, parent, false);

        return new ViewHolder(contactView, context);
    }

    @Override
    public void onBindViewHolder(FarmListAdapter.ViewHolder holder, int position) {
        holder.setView(mFilteredFarms.get(position));
    }

    @Override
    public int getItemCount() {
        return mFilteredFarms.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mListener = onItemClickListener;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mFilteredFarms = (List<Farm>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                List<Farm> filteredResults = getFilteredResults(constraint);

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }

            private List<Farm> getFilteredResults(CharSequence constraint) {

                List<Farm> filtered = new ArrayList<>();

                for (Farm farm : mAllFarms) {

                    String name = Normalizer.normalize(farm.getName(), Normalizer.Form.NFD);
                    name = name.replaceAll("[^\\p{ASCII}]", "").toLowerCase(Locale.US);

                    if (name.contains(constraint)) filtered.add(farm);
                }

                return filtered;
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView titleTextView;
        private TextView subtitleTextView;
        private TextView distanceView;
        private LinearLayout productsLayout;

        private Context context;

        public ViewHolder(View itemView, Context context) {
            super(itemView);

            this.context = context;

            titleTextView = (TextView) itemView.findViewById(R.id.title);
            subtitleTextView = (TextView) itemView.findViewById(R.id.subtitle);
            productsLayout = (LinearLayout) itemView.findViewById(R.id.layout_products);
            distanceView = (TextView) itemView.findViewById(R.id.distance);
            itemView.findViewById(R.id.btn_detail).setOnClickListener(this);

        }

        public void setView(Farm farm) {
            titleTextView.setText(farm.getName().toUpperCase());
            subtitleTextView.setText(farm.getAddressList());
            if (!TextUtils.isEmpty(farm.getDistanceInMiles())) {
                distanceView.setText(String.format(context.getResources().getString(R.string.distance_list), farm.getDistanceInMiles()));
            }

            productsLayout.removeAllViews();

            for (String product : farm.getProducts()) {
                ImageView productImage = new ImageView(context);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(Utils.getPixels(context, 25), ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0, 0, Utils.getPixels(context, 3), 0);
                lp.gravity = Gravity.BOTTOM;
                productImage.setLayoutParams(lp);
                productImage.setImageResource(Product.getProductRes(product));
                productImage.setAlpha(0.8f);
                productsLayout.addView(productImage);
            }
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(mFilteredFarms.get(getAdapterPosition()));
        }
    }
}