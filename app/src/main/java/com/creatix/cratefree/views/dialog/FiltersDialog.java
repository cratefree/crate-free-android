package com.creatix.cratefree.views.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.MaterialMenuView;
import com.creatix.cratefree.R;
import com.creatix.cratefree.api.objects.Category;
import com.creatix.cratefree.api.objects.Product;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnFiltersSelectedListener;
import com.creatix.cratefree.utils.listeners.OnScrolledListener;
import com.creatix.cratefree.views.CustomRecyclerView;
import com.creatix.cratefree.views.lists.adapters.FiltersAdapter;

import java.util.List;

/**
 * Created by jano on 8.12.2015.
 */
public class FiltersDialog extends FullScreenDialog implements DialogInterface.OnDismissListener {

    private RelativeLayout toolbar;
    private ViewGroup toolbarHolder;
    private View shadow;

    private CustomRecyclerView filtersList;
    private FiltersAdapter filtersAdapter;

    private MaterialMenuView homeMenu;

    private Context context;

    private OnFiltersSelectedListener onDialogClosedListener;

    public FiltersDialog(final Context context, OnFiltersSelectedListener onFiltersSelectedListener, List<Product> selectedProducts, List<Category> selectedCategories) {
        super(context, R.layout.layout_dialog_filters, R.style.ThemeDialogCustom);

        this.context = context;
        this.onDialogClosedListener = onFiltersSelectedListener;

        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        toolbarHolder = (FrameLayout) findViewById(R.id.toolbar_holder);
        filtersList = (CustomRecyclerView) findViewById(R.id.filters_list);
        shadow = findViewById(R.id.toolbar_shadow);
        homeMenu = (MaterialMenuView) findViewById(R.id.btn_back);

        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) toolbar.getLayoutParams();
//        lp.setMargins(0, Utils.getStatusBarHeight(context), 0, 0);

        filtersAdapter = new FiltersAdapter(this);
        filtersAdapter.setSelectedLists(selectedProducts, selectedCategories);

        filtersList.setAdapter(filtersAdapter);
        filtersList.setOnScrolledListener(new OnScrolledListener() {

            @Override
            public void onScrolled(int scrolled) {
                elavateToolbar(scrolled != 0);
            }
        });

        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        homeMenu.setState(MaterialMenuDrawable.IconState.ARROW);
        homeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        setOnDismissListener(this);
    }

    private void elavateToolbar(boolean elevate) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbarHolder.setElevation(elevate ? Utils.getPixels(context, 5) : 0);
        } else {
            shadow.setVisibility(elevate ? View.VISIBLE : View.GONE);
        }

        toolbarHolder.setBackgroundColor(context.getResources().getColor(elevate ? R.color.white : R.color.white_dirty));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (onDialogClosedListener != null) {
            onDialogClosedListener.OnFiltersSelected(filtersAdapter.getSelectedProducts(), filtersAdapter.getSelectedCategories());
        }
    }
}
