package com.creatix.cratefree.views;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by jano on 5.12.2015.
 */
public class NonTouchableCoordinatorLayout extends CoordinatorLayout {
    public NonTouchableCoordinatorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }
}
