package com.creatix.cratefree.views;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.UiUtil;

public class FontableRadioButton extends AppCompatRadioButton {

    public FontableRadioButton(Context context) {
        super(context);
    }

    public FontableRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_creatix_cratefree_views_FontableRadioButton,
                R.styleable.com_creatix_cratefree_views_FontableRadioButton_fontName);
    }

    public FontableRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.com_creatix_cratefree_views_FontableRadioButton,
                R.styleable.com_creatix_cratefree_views_FontableRadioButton_fontName);
    }
}