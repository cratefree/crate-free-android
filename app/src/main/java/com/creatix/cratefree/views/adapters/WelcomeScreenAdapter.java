package com.creatix.cratefree.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.listeners.TouchActionListener;
import com.creatix.cratefree.views.FontableTextView;
import com.creatix.cratefree.views.GetStartedLayout;

import java.util.ArrayList;

/**
 * Created by Jano on 16. 1. 2015.
 */
public class WelcomeScreenAdapter extends PagerAdapter {

    private ArrayList<View> views = new ArrayList<>();

    public WelcomeScreenAdapter(final Context context) {

        GetStartedLayout walkthrough1 = new GetStartedLayout(context, R.layout.layout_walkthrough_1);
        this.views.add(walkthrough1);

        TextView splashTitleTextView = (TextView) walkthrough1.findViewById(R.id.title);

        Spannable splashTitle = new SpannableString(context.getResources().getString(R.string.label_say_no));
        splashTitle.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.red)), 4, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        splashTitleTextView.setText(splashTitle);

        GetStartedLayout walkthrough2 = new GetStartedLayout(context, R.layout.layout_walkthrough_2);
        this.views.add(walkthrough2);

        GetStartedLayout walkthrough3 = new GetStartedLayout(context, R.layout.layout_walkthrough_3);
        this.views.add(walkthrough3);

        FontableTextView btnSubscribe = (FontableTextView) walkthrough3.findViewById(R.id.btn_subscribe);
        btnSubscribe.registerClickableTouchListener(new TouchActionListener() {
            @Override
            public void onTouch() {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://cratefreeil.us11.list-manage.com/subscribe?u=42c3d81fff809c3be687a1b55&id=e1f93b7278"));
                context.startActivity(browserIntent);
            }
        });
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = views.get(position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }
}
