package com.creatix.cratefree.views.lists.adapters;

import android.content.Context;
import android.widget.LinearLayout;

import java.util.HashMap;

/**
 * Created by jano on 11.12.2015.
 */
public interface FormLayout {

    public HashMap<String, Object> getParams();

    public boolean isValid();
}
