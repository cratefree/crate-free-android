package com.creatix.cratefree.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.creatix.cratefree.BaseActivity;
import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.Utils;

public class CustomToolbar extends RelativeLayout {

    private ProgressBar progressBar;
    private BaseActivity activity;

    public CustomToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        this.activity = (BaseActivity) context;
        inflate(context, R.layout.toolbar, this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        setDefaultPosition();
    }

    public void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        progressBar.setVisibility(View.VISIBLE);
        progressBar.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
    }

    public void setPositionToTop() {
        progressBar.setPadding(0, 0, 0, 0);
    }

    public void setDefaultPosition() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setPadding(0, Utils.getActionBarHeight(activity) - Utils.getPixels(activity, 6), 0, 0);
        } else {
            progressBar.setPadding(0, Utils.getActionBarHeight(activity), 0, 0);
        }
    }
}