package com.creatix.cratefree.views.lists.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.creatix.cratefree.R;
import com.creatix.cratefree.api.objects.Category;
import com.creatix.cratefree.api.objects.Product;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnItemClickListener;
import com.creatix.cratefree.views.dialog.FiltersDialog;
import com.creatix.cratefree.views.lists.adapters.items.FilterHolder;
import com.creatix.cratefree.views.lists.adapters.items.HeaderHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnItemClickListener {

    private Context context;
    private FiltersDialog filtersDialog;

    private List<Product> products;
    private List<Product> selectedProducts = new ArrayList<>();

    private List<Category> categories;
    private List<Category> selectedCategories = new ArrayList<>();

    private static final int CATEGORY_VIEW = 0;
    private static final int PRODUCT_VIEW = 1;
    private static final int CATEGORY_HEADER = 2;
    private static final int PRODUCT_HEADER = 3;
    private static final int FOOTER = 4;

    private static final int FILTER_TYPES_COUNT = 2;

    protected OnItemClickListener mListener;

    private Snackbar snackbar;

    private List<FilterHolder> filterItems;

    public FiltersAdapter(FiltersDialog dialog) {

        this.filtersDialog = dialog;
        this.context = filtersDialog.getContext();
        this.filterItems = new ArrayList<>();

        this.products = Arrays.asList(Product.values());
        this.categories = Arrays.asList(Category.values());

        setOnItemClickListener(this);

        initRemoveAllBar();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        if (viewType == PRODUCT_VIEW) {

            View view = inflater.inflate(R.layout.layout_filters_item, parent, false);
            FilterHolder filterHolder =  new FilterHolder<>(view, context, categories.size() + FILTER_TYPES_COUNT, this, products, selectedProducts);
            filterItems.add(filterHolder);

            return filterHolder;
        } else if (viewType == CATEGORY_VIEW) {

            View view = inflater.inflate(R.layout.layout_filters_item, parent, false);
            FilterHolder filterHolder =  new FilterHolder<>(view, context, 1, this, categories, selectedCategories);
            filterItems.add(filterHolder);

            return filterHolder;
        } else if (viewType == PRODUCT_HEADER) {

            View view = inflater.inflate(R.layout.layout_filters_header, parent, false);
            return new HeaderHolder(view, context, R.string.label_products);
        } else if (viewType == CATEGORY_HEADER) {

            View view = inflater.inflate(R.layout.layout_filters_header, parent, false);
            return new HeaderHolder(view, context, R.string.label_categories);
        } else {
            View view = new View(context);
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.white_dirty));
            view.setLayoutParams(new RecyclerView.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, Utils.getPixels(context, 50)));
            return new RecyclerView.ViewHolder(view) {
                @Override
                public String toString() {
                    return super.toString();
                }
            };
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? CATEGORY_HEADER :
                position <= categories.size() ? CATEGORY_VIEW :
                        position == categories.size() + 1 ? PRODUCT_HEADER :
                                position > categories.size() + 1 && position < categories.size() + products.size() + FILTER_TYPES_COUNT ? PRODUCT_VIEW : FOOTER;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position > 0 && position <= categories.size()) {
            ((FilterHolder) holder).setView(position - 1);
        } else if (position > categories.size() + 1 && position < categories.size() + products.size() + FILTER_TYPES_COUNT) {
            ((FilterHolder) holder).setView(position - categories.size() - FILTER_TYPES_COUNT);
        }
    }

    @Override
    public int getItemCount() {
        return categories.size() + products.size() + FILTER_TYPES_COUNT + 1;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mListener = onItemClickListener;
    }

    public List<Product> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedLists(List<Product> selectedProducts, List<Category> selectedCategories) {
        if (selectedProducts != null) {
            this.selectedProducts = selectedProducts;
        }
        if (selectedCategories != null) {
            this.selectedCategories = selectedCategories;
        }

        toggleSnackBar();
    }

    public List<Category> getSelectedCategories() {
        return selectedCategories;
    }

    @Override
    public void onItemClick(Object object) {

        if (object instanceof Product) {

            Product selectedProduct = (Product) object;

            if (selectedProducts.contains(selectedProduct)) {
                selectedProducts.remove(selectedProduct);
            } else {
                selectedProducts.add(selectedProduct);
            }
        } else if (object instanceof Category) {

            Category selectedCategory = (Category) object;

            if (selectedCategories.contains(selectedCategory)) {
                selectedCategories.remove(selectedCategory);
            } else {
                selectedCategories.add(selectedCategory);
            }
        }

        toggleSnackBar();
    }

    private void initRemoveAllBar() {

        snackbar = Snackbar
                .make(filtersDialog.findViewById(R.id.content), "", Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(ContextCompat.getColor(context, R.color.blue_lt));
        snackbar.setAction(context.getString(R.string.btn_remove_filters), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
                clearFilters();
            }
        });
    }

    private void toggleSnackBar() {
        if (selectedCategories.isEmpty() && selectedProducts.isEmpty()) {
            snackbar.dismiss();
        } else {
            snackbar.show();
        }
    }

    private void clearFilters() {

        selectedCategories.clear();
        selectedProducts.clear();

        for(FilterHolder item: filterItems) {
            item.unselect();
        }
    }
}