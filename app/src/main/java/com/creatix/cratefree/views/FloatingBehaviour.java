package com.creatix.cratefree.views;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

import com.creatix.cratefree.utils.Utils;

/**
 * Created by jano on 2.12.2015.
 */
public class FloatingBehaviour extends CoordinatorLayout.Behavior<ImageButton> {
    private int toolbarHeight;

    public FloatingBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.toolbarHeight = Utils.getActionBarHeight(context);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageButton fab, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageButton child, View dependency) {
        System.out.println(dependency + "" + parent);
        return super.onDependentViewChanged(parent, child, dependency);
    }
}
