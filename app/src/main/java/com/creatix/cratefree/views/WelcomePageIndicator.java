package com.creatix.cratefree.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.creatix.cratefree.R;

public class WelcomePageIndicator extends LinearLayout {

    protected ImageView page1;
    protected ImageView page2;
    protected ImageView page3;

    public WelcomePageIndicator(Context context) {
        super(context);
        // TODO Auto-generated constructor stub

        inflate(context, R.layout.layout_welcome_page_indicator, this);
    }

    public WelcomePageIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub

        inflate(context, R.layout.layout_welcome_page_indicator, this);
        page1 = (ImageView) findViewById(R.id.indicator_page_1);
        page2 = (ImageView) findViewById(R.id.indicator_page_2);
        page3 = (ImageView) findViewById(R.id.indicator_page_3);

    }

    public void selectPage(int position) {
        switch (position) {
            case 0:
                page1.setImageResource(R.drawable.dot_green);
                page2.setImageResource(R.drawable.dot_white);
                page3.setImageResource(R.drawable.dot_white);
                break;
            case 1:
                page1.setImageResource(R.drawable.dot_white);
                page2.setImageResource(R.drawable.dot_green);
                page3.setImageResource(R.drawable.dot_white);
                break;
            case 2:
                page1.setImageResource(R.drawable.dot_grey);
                page2.setImageResource(R.drawable.dot_grey);
                page3.setImageResource(R.drawable.dot_green);
                break;
        }
    }
}
