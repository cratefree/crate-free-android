package com.creatix.cratefree.views;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.views.lists.adapters.FormLayout;

import java.util.HashMap;


public class ContactFarmLayout extends LinearLayout implements FormLayout {

    private EditText emailText;
    private EditText farmNameText;
    private EditText farmContactText;
    private EditText commentText;
    
    private String email;
    private String farmName;
    private String farmContact;
    private String comment;

    public ContactFarmLayout(Context context) {
        super(context);
        inflate(context, R.layout.layout_suggest_farm, this);

        emailText = (EditText) findViewById(R.id.edittext_email);
        farmNameText = (EditText) findViewById(R.id.edittext_farm_name);
        farmContactText = (EditText) findViewById(R.id.edittext_farm_contact);
        commentText = (EditText) findViewById(R.id.edittext_comments);

    }

    @Override
    public HashMap<String, Object> getParams() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("contact", email);
        params.put("farmName", farmName);
        params.put("farmContact", farmContact);
        params.put("text", comment);

        return params;
    }

    @Override
    public boolean isValid() {
        
        email = emailText.getText().toString();
        farmName = farmNameText.getText().toString();
        farmContact = farmContactText.getText().toString();
        comment = commentText.getText().toString();

        if(!Utils.isEmailValid(email)) {
            Utils.showToast(getContext(), getResources().getString(R.string.email_invalid));
            return false;
        } else if(farmName == null || farmName.length() < 3) {
            Utils.showToast(getContext(), getResources().getString(R.string.farm_name_invalid));
            return false;
        } else if(farmContact == null || farmContact.length() < 3) {
            Utils.showToast(getContext(), getResources().getString(R.string.contact_invalid));
            return false;
        }
        
        return true;
    }
}
