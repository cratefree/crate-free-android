package com.creatix.cratefree.views;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.creatix.cratefree.R;
import com.creatix.cratefree.api.objects.Farm;

import java.util.List;


public class SocialLayout extends LinearLayout {

    private ImageButton btnTwitter;
    private ImageButton btnInstagram;
    private ImageButton btnFacebook;

    private Context context;

    public SocialLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SocialLayout(Context context, Farm farm) {
        super(context);
        inflate(context, R.layout.layout_social_contact, this);

        this.context = context;

        btnTwitter = (ImageButton) findViewById(R.id.btn_twitter);
        btnInstagram = (ImageButton) findViewById(R.id.btn_instagram);
        btnFacebook = (ImageButton) findViewById(R.id.btn_facebook);

        setSocialButtons(farm);
    }

    public void setSocialButtons(final Farm farm) {

        if (!TextUtils.isEmpty(farm.getFacebook())) {
            btnFacebook.setVisibility(VISIBLE);
            btnFacebook.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    openUrl(farm.getFacebook(), "com.facebook.katana");
                }
            });
        }

        if (!TextUtils.isEmpty(farm.getInstagram())) {
            btnInstagram.setVisibility(VISIBLE);
            btnInstagram.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    openUrl(farm.getInstagram(), "com.instagram.android");
                }
            });
        }

        if (!TextUtils.isEmpty(farm.getTwitter())) {
            btnTwitter.setVisibility(VISIBLE);
            btnTwitter.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    openUrl(farm.getTwitter(), "com.twitter.android");
                }
            });
        }
    }

    private void openUrl(String url, String packageString) {
        Uri uri = Uri.parse(validateUrl(url));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage(packageString);

        if (isIntentAvailable(intent)) {
            context.startActivity(intent);
        } else {
            context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    }

    private boolean isIntentAvailable(Intent intent) {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private String validateUrl(String url) {
        return url.toLowerCase().contains("http://") || url.toLowerCase().contains("https://") ? url : "http://" + url;
    }
}
