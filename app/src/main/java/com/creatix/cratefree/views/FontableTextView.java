package com.creatix.cratefree.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.UiUtil;
import com.creatix.cratefree.utils.listeners.TouchActionListener;


public class FontableTextView extends AppCompatTextView {

    private TouchActionListener touchActionListener;
    private OnTouchListener clickableTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // TODO Auto-generated method stub

            Rect rect = new Rect(v.getLeft(), v.getTop(), v.getRight(),
                    v.getBottom());
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                setTextColor(getResources().getColor(R.color.red));

            } else if (event.getAction() == MotionEvent.ACTION_MOVE
                    || event.getAction() == MotionEvent.ACTION_CANCEL) {
                if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop()
                        + (int) event.getY())
                        || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    setTextColor(getResources().getColor(R.color.blue_lt));
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {

                if (rect.contains(v.getLeft() + (int) event.getX(), v.getTop()
                        + (int) event.getY())) {
                    setTextColor(getResources().getColor(R.color.blue_lt));

                    touchActionListener.onTouch();
                }
            }
            return true;

        }
    };

    public FontableTextView(Context context) {
        super(context);
    }

    public FontableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(
                this,
                context,
                attrs,
                R.styleable.com_creatix_cratefree_views_FontableTextView,
                R.styleable.com_creatix_cratefree_views_FontableTextView_fontName);
    }

    public FontableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(
                this,
                context,
                attrs,
                R.styleable.com_creatix_cratefree_views_FontableTextView,
                R.styleable.com_creatix_cratefree_views_FontableTextView_fontName);
    }

    public void registerClickableTouchListener(
            TouchActionListener touchActionListener) {

        this.touchActionListener = touchActionListener;
        setOnTouchListener(clickableTouchListener);

    }
}