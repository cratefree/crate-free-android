package com.creatix.cratefree.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;


public class GetStartedLayout extends RelativeLayout {

    public GetStartedLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GetStartedLayout(Context context, int layoutId) {
        super(context);
        View v = inflate(context, layoutId, this);
    }
}
