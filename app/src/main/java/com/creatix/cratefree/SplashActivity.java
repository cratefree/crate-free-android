package com.creatix.cratefree;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import android.widget.Toast;
import com.creatix.cratefree.api.calls.BaseCall;
import com.creatix.cratefree.api.calls.FarmsListCall;
import com.creatix.cratefree.gcm.RegistrationIntentService;
import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnTaskCompleteListener;

public class SplashActivity extends BaseActivity {

    protected BaseCall apiCall;

    private boolean isGooglePlayOK = false;

    private boolean openedSettings = false;
    private boolean openedLocation = false;

    private boolean checkingGPS = false;
    private boolean toCheckGPS = true;

    private final int LOCATION_PERMISSION_REQUEST_CALLBACK_ID = 99;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView splashTitleTextView = (TextView) findViewById(R.id.title_splash);

        Spannable splashTitle = new SpannableString(getString(R.string.splash_title2));
        splashTitle.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 8, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        splashTitleTextView.setText(splashTitle);

        new Thread() {

            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (hasPermissions()) {
                        continueAndCheckGps();
                    }
                }
            }
        }.start();
    }

    private void continueAndCheckGps() {
        if (!checkingGPS && toCheckGPS) {
            checkGps();
        } else {
            // continue with checking play services
            if (!isGooglePlayOK) {
                continueSplash();
            }
        }
    }

    private void checkGps() {

        checkingGPS = true;
        toCheckGPS = false;

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    showGPSdialog();
                }
            });
        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    continueSplash();
                }
            });
        }
    }

    private void showGPSdialog() {

        new AlertDialog.Builder(this).setTitle(R.string.dialog_gps_title).setMessage(R.string.dialog_gps_message).setCancelable(false).setPositiveButton(R.string.dialog_btn_positive, new DialogInterface.OnClickListener() {

            public void onClick(final DialogInterface dialog, final int id) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                checkingGPS = false;
                openedLocation = true;
            }
        }).setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {

            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
                checkingGPS = false;
                continueSplash();
            }
        }).create().show();

    }

    private boolean hasPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                return true;
            } else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    showAskLocationPermissionDialog();
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[] {
                        Manifest.permission.ACCESS_FINE_LOCATION
                    }, LOCATION_PERMISSION_REQUEST_CALLBACK_ID);
                }

                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == LOCATION_PERMISSION_REQUEST_CALLBACK_ID) {

            boolean permissionDenied = false;

            for (int granted : grantResults) {
                if (granted == PackageManager.PERMISSION_DENIED) {
                    permissionDenied = true;
                }
            }

            if (permissionDenied) {
                showAskLocationPermissionDialog();
            } else {
                continueAndCheckGps();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (openedSettings){
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED ||
                    ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED){
                Toast.makeText(SplashActivity.this, R.string.location_permission_denied, Toast.LENGTH_LONG).show();
                SplashActivity.this.finish();
            }
            else {
                continueAndCheckGps();
            }
        }

        if (openedLocation){
            continueSplash();
        }
    }

    private void showAskLocationPermissionDialog() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                new AlertDialog.Builder(SplashActivity.this)
                    .setTitle(R.string.dialog_location_permission)
                    .setMessage(R.string.dialog_location_message)
                    .setCancelable(false)
                    .setPositiveButton(R.string.dialog_btn_settings, new DialogInterface.OnClickListener() {

                        public void onClick(final DialogInterface dialog, final int id) {
                            Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i.addCategory(Intent.CATEGORY_DEFAULT);
                            i.setData(Uri.parse("package:" + getPackageName()));
                            openedSettings = true;
                            startActivity(i);
                        }
                    }).setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int id) {
                        Toast.makeText(SplashActivity.this, R.string.location_permission_denied, Toast.LENGTH_LONG).show();
                        SplashActivity.this.finish();
                    }
                }).create().show();
            }
        });
    }

    private void continueSplash() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                if (Utils.checkPlayServices(SplashActivity.this)) {
                    // If this check succeeds, proceed with normal processing.
                    // Otherwise, prompt user to get valid Play Services APK.
                    isGooglePlayOK = true;
                    if (MyApplication.getInstance().updateApp()) {
                        loadFarms();
                    } else {
                        proceed();
                    }
                }
            }
        });
    }

    private void proceed() {
        if (Utils.checkPlayServices(this)) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            isGooglePlayOK = true;

            ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(this, R.anim.fade_in, android.R.anim.fade_out);
            Intent intent;
            if (MyApplication.getInstance().skipWalktrough()) {

                intent = new Intent(this, HomeActivity.class);

                if (getIntent().getExtras() != null) {
                    intent.putExtras(getIntent().getExtras());
                }
            } else {
                intent = new Intent(this, WalkthroughActivity.class);
            }

            try {
                ActivityCompat.startActivity(this, intent, options.toBundle());
                ActivityCompat.finishAfterTransition(this);
            } catch (Exception ex) {
                startActivity(intent);
            }

            if (MyApplication.getInstance().getPrefs().getBoolean(Prefs.NOTIFICATIONS_ENABLED, true) && TextUtils.isEmpty(MyApplication.getInstance().getPrefs().getString(Prefs.GCM_TOKEN, ""))) {
                startService(new Intent(this, RegistrationIntentService.class));
            }
        }
    }

    private void loadFarms() {

        apiCall = new FarmsListCall(this);
        apiCall.setOnTaskCompleteListener(new OnTaskCompleteListener() {

            @Override
            public void OnTaskComplete(Object result) {
                proceed();
            }
        });
        apiCall.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
