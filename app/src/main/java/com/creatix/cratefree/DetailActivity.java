package com.creatix.cratefree;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.api.objects.Product;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.TouchActionListener;
import com.creatix.cratefree.views.ContactLayout;
import com.creatix.cratefree.views.SocialLayout;

public class DetailActivity extends BaseActivity {

    private ImageButton menuBack;

    private TextView nameView;
    private TextView streetView;
    private TextView addressView;
    private TextView distanceView;
    private TextView typeView;
    private TextView productsView;

    private LinearLayout productsLayout;
    private LinearLayout contactLayout;

    private final int CALL_PHONE_REQUEST_ID = 100;

    private Farm farm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getExtras() == null) {
            finish();
        }

        this.farm = (Farm) getIntent().getExtras().getSerializable("farm");

        initViews();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_detail;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Utils.makePhoneCall(farm.getPhone(), DetailActivity.this);
        }
    }

    protected void initViews() {

        nameView = (TextView) findViewById(R.id.title_name);
        streetView = (TextView) findViewById(R.id.title_street);
        addressView = (TextView) findViewById(R.id.title_address);
        distanceView = (TextView) findViewById(R.id.title_distance);
        typeView = (TextView) findViewById(R.id.title_type);
        productsView = (TextView) findViewById(R.id.title_products);

        productsLayout = (LinearLayout) findViewById(R.id.layout_products);
        contactLayout = (LinearLayout) findViewById(R.id.contact_holder);

        menuBack = (ImageButton) findViewById(R.id.btn_back);

        menuBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setData(streetView, farm.getStreet());
        setData(addressView, farm.getAddress());
        setData(typeView, farm.getTypesString());
        setData(productsView, farm.getProductsString(res));

        nameView.setText(farm.getName());

        if (!TextUtils.isEmpty(farm.getDistanceInMiles())) {
            distanceView.setText(String.format(getString(R.string.distance_detail), farm.getDistanceInMiles()));
            findViewById(R.id.label_distance).setVisibility(View.VISIBLE);
        } else {
            distanceView.setVisibility(View.GONE);
        }

        if (farm.getProducts().size() != 0) {
            for (int i = 0; i < farm.getProducts().size(); i++) {
                ImageView productImage = new ImageView(this);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, Utils.getPixels(this, 25));
                if (i < farm.getProducts().size() - 1) {
                    lp.setMargins(0, 0, Utils.getPixels(this, 10), 0);
                }
                productImage.setLayoutParams(lp);
                productImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
                productImage.setImageResource(Product.getProductRes(farm.getProducts().get(i)));
                productsLayout.addView(productImage);
            }
        } else {
            productsLayout.setVisibility(View.GONE);
            productsView.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(farm.getLatitude()) && !TextUtils.isEmpty(farm.getLongitude())) {

            ImageButton btnDirections = (ImageButton) findViewById(R.id.btn_directions);
            btnDirections.setVisibility(View.VISIBLE);
            btnDirections.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.navigateTo(DetailActivity.this, farm.getLatitude(), farm.getLongitude());
                }
            });
        }

        if (!TextUtils.isEmpty(farm.getPerson())) {
            ContactLayout nameLayout = new ContactLayout(this);
            nameLayout.setViews(getString(R.string.label_contact), farm.getPerson());
            contactLayout.addView(nameLayout);
        }

        if (!TextUtils.isEmpty(farm.getEmail())) {
            ContactLayout emailLayout = new ContactLayout(this);
            emailLayout.setViews(getString(R.string.label_email), farm.getEmail());
            emailLayout.setOnClickListener(new TouchActionListener() {
                @Override
                public void onTouch() {
                    Utils.sendEmail(farm.getEmail(), DetailActivity.this);
                }
            });
            contactLayout.addView(emailLayout);
        }

        if (!TextUtils.isEmpty(farm.getPhone())) {
            ContactLayout phoneLayout = new ContactLayout(this);
            phoneLayout.setViews(getString(R.string.label_phone), farm.getPhone());
            phoneLayout.setOnClickListener(new TouchActionListener() {
                @Override
                public void onTouch() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(DetailActivity.this, new String[] {
                                Manifest.permission.CALL_PHONE}, CALL_PHONE_REQUEST_ID);
                    }
                    else {
                        Utils.makePhoneCall(farm.getPhone(), DetailActivity.this);
                    }
                }
            });
            contactLayout.addView(phoneLayout);
        }

        if (!TextUtils.isEmpty(farm.getURL())) {
            ContactLayout websiteLayout = new ContactLayout(this);
            websiteLayout.setViews(getString(R.string.label_website), farm.getURL());
            websiteLayout.setOnClickListener(new TouchActionListener() {
                @Override
                public void onTouch() {
                    String url = (farm.getURL().toLowerCase().contains("http://") || farm.getURL().toLowerCase().contains("https://")) ? farm.getURL() : "http://" + farm.getURL();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
            });
            contactLayout.addView(websiteLayout);
        }

        if (farm.hasSocial()) {
            SocialLayout socialLayout = new SocialLayout(this, farm);
            contactLayout.addView(socialLayout);
        }
    }

    private void setData(TextView textView, String data) {
        if (!TextUtils.isEmpty(data)) {
            textView.setText(data);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
