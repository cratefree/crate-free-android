package com.creatix.cratefree.fragments.utils;

/**
 * Created by jano on 24.11.2015.
 */
public class FragmentTags {

    public static final String SPLASH = "SPLASH";
    public static final String WALKTHROUGH = "WALKTHROUGH";
    public static final String MAP = "MAP";
    public static final String DETAIL = "DETAIL";
}
