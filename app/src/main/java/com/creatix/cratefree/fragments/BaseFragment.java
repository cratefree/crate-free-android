package com.creatix.cratefree.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.creatix.cratefree.BaseActivity;
import com.creatix.cratefree.api.calls.BaseCall;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.FragmentsActionsListener;
import com.creatix.cratefree.utils.listeners.OnFragmentFinishListener;
import com.creatix.cratefree.views.CustomToolbar;

public abstract class BaseFragment extends Fragment {

    protected FragmentsActionsListener mListener;
    protected BaseActivity activity;

    protected BaseCall apiCall;

    protected View v;

    protected View mContentView;
    protected View mProgressView;

    protected boolean isAttached = false;

    protected OnFragmentFinishListener mFinishListener;

    protected View emptyView;

    protected boolean justStarted = true;
    protected boolean progressShown = false;

    protected CustomToolbar toolbar;

    protected Resources resources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        this.activity = (BaseActivity) getActivity();
        this.resources = activity.getResources();
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        if (apiCall != null && apiCall.getStatus() != AsyncTask.Status.FINISHED) {
            apiCall.toggleProgress();
        }

        if (justStarted) {
            justStarted = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

//        if (activity.findViewById(R.id.paynut_toolbar) != null) {
//            toolbar = (PaynutToolbar) activity.findViewById(R.id.paynut_toolbar);
//        } else if (v.findViewById(R.id.toolbar_layout) != null) {
//            toolbar = (PaynutToolbar) v.findViewById(R.id.toolbar_layout);
//        }
//        if (toolbar != null) {
//            toolbar.setDefaultPosition();
//        }

        initActionBar();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                setActionBar();
            }
        });
        initViews();
        initViewsWithData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isAttached = true;

        try {
            mListener = (FragmentsActionsListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        isAttached = false;

        cancelActions();
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        cancelActions();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        cancelActions();

    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();

        cancelActions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                activity.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void cancelActions() {
        try {
            Utils.hideKeyboard(activity, activity.getCurrentFocus());
        } catch (Exception e) {
        }

        if (apiCall != null) apiCall.cancel(true);

    }

    protected void loadData() {

//		((HomeActivity) activity).toggleDrawer(false);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    protected void showProgress(final boolean show) {
        if (!isAdded() || (!show && !progressShown)) return;

        progressShown = show;

        int shortAnimTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        mProgressView.setVisibility(View.VISIBLE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });

        mContentView.setVisibility(View.VISIBLE);
        mContentView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mContentView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

    }

    public void setOnFragmentFinishListener(OnFragmentFinishListener listener) {
        this.mFinishListener = listener;
    }


    protected void initViews() {
        if (!isAdded() || isDetached()) {
            return;
        }
    }

    protected void initViewsWithData() {
        if (!isAdded() || isDetached()) {
            return;
        }

        loadData();
    }

    protected abstract void initActionBar();

    protected void setActionBar() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
