package com.creatix.cratefree.fragments.utils;

/**
 * Created by jano on 24.11.2015.
 */
public class FragmentActions {

    public static final int SPLASH = 0;
    public static final int WALKTHROUGH = 1;
    public static final int MAP = 2;
    public static final int DETAIL = 3;
}
