package com.creatix.cratefree.utils.listeners;

public interface OnBackPressedListener {
   void onBackPressed();
 }
