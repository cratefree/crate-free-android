package com.creatix.cratefree.utils.listeners;

import android.os.Bundle;

public interface FragmentsActionsListener {

    void onFragmentAction(int action, Bundle bundle, OnFragmentFinishListener listener);
}
