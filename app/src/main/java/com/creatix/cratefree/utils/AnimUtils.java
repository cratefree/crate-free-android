package com.creatix.cratefree.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.creatix.cratefree.R;

/**
 * Created by jano on 14.12.2015.
 */
public class AnimUtils {

    public static void toggleGrowAnim(Context context, final View view, boolean show) {

        final Animation inAnim = AnimationUtils.loadAnimation(context, R.anim.grow_anim);
        final Animation outAnim = AnimationUtils.loadAnimation(context, R.anim.shrink_anim);

        animate(view, show, show ? inAnim : outAnim);
    }

    public static void toggleFade(Context context, final View view, boolean show) {

        final Animation inAnim = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        final Animation outAnim = AnimationUtils.loadAnimation(context, R.anim.fade_out);

        animate(view, show, show ? inAnim : outAnim);
    }

    public static void toggleSlideBottom(Context context, final View view, boolean show) {

        final Animation inAnim = AnimationUtils.loadAnimation(context, R.anim.slide_in_bottom);
        final Animation outAnim = AnimationUtils.loadAnimation(context, R.anim.slide_out_bottom);

        animate(view, show, show ? inAnim : outAnim);
    }

    private static void animate(final View view, final boolean show, Animation anim) {

        if (view.isEnabled()) {

            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    view.setVisibility(View.VISIBLE);
                    view.setEnabled(false);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(show ? View.VISIBLE : View.GONE);
                    view.setEnabled(true);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });

            view.startAnimation(anim);
        }
    }
}
