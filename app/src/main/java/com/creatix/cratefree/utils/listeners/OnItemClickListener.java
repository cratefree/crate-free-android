package com.creatix.cratefree.utils.listeners;

public interface OnItemClickListener {
   void onItemClick(Object object);
 }
