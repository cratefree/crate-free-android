package com.creatix.cratefree.utils.listeners;

public interface OnTaskCompleteListener {

    void OnTaskComplete(Object result);

}
