package com.creatix.cratefree.utils.listeners;

public interface OnScrolledListener {
   void onScrolled(int scrolled);
 }
