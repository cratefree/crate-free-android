package com.creatix.cratefree.utils;

/**
 * Created by jano on 25.11.2015.
 */
public class Prefs {

    public static final String SKIP_WALKTHROUGH = "skip_walkthrough";
    public static final String LAST_UPDATED = "last_updated";

    public static final String SENT_TOKEN_TO_SERVER = "token_sent";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String NOTIFICATIONS_ENABLED = "notifications_enabled";

    public static final String DEVICE_UUID = "uuid";
}
