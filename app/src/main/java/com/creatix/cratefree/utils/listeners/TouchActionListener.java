package com.creatix.cratefree.utils.listeners;

public interface TouchActionListener {

    void onTouch();

}
