package com.creatix.cratefree.utils;

public class Constants {

    public static final String TAG_RETRIEVER = "CrateFree IL";
    public static final String TAG_DATABASE = "SQL Lite";

    public static final String SHARE_APP = "https://play.google.com/store/apps/details?id=com.creatix.cratefree";

    public static final String DELIMITER = "---";

}
