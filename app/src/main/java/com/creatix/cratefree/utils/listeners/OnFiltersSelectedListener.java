package com.creatix.cratefree.utils.listeners;

import com.creatix.cratefree.api.objects.Category;
import com.creatix.cratefree.api.objects.Product;

import java.util.List;

public interface OnFiltersSelectedListener {

    void OnFiltersSelected(List<Product> products, List<Category> categories);

}
