package com.creatix.cratefree;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.listeners.TouchActionListener;
import com.creatix.cratefree.views.CreatixButton;
import com.creatix.cratefree.views.FontableTextView;
import com.creatix.cratefree.views.WelcomePageIndicator;
import com.creatix.cratefree.views.adapters.WelcomeScreenAdapter;

public class WalkthroughActivity extends BaseActivity {

    private WelcomePageIndicator pageIndicator;
    private WelcomeScreenAdapter screenAdapter;
    private ViewPager viewPager;
    private CreatixButton btnNext;
    private FontableTextView btnSkip;
    private TextView privacyPolicyTextView;

    @Override
    protected void onResume() {
        super.onResume();

        initViews();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_walkthrough;
    }

    protected void initViews() {
        screenAdapter = new WelcomeScreenAdapter(this);
        pageIndicator = (WelcomePageIndicator) findViewById(R.id.page_indicator);
        viewPager = (ViewPager) findViewById(R.id.pager);
        btnNext = (CreatixButton) findViewById(R.id.btn_next);
        btnSkip = (FontableTextView) findViewById(R.id.btn_skip);
        privacyPolicyTextView = findViewById(R.id.privacyPolicyWalkthrough);

        viewPager.setAdapter(screenAdapter);
        viewPager.addOnPageChangeListener(pagerListener);
        btnNext.setOnClickListener(btnNextListener);
        btnSkip.registerClickableTouchListener(btnSkipListener);
        privacyPolicyTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private View.OnClickListener btnNextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (viewPager.getCurrentItem() == 2) {
                proceed();
            } else {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            }
        }
    };

    private TouchActionListener btnSkipListener = new TouchActionListener() {

        @Override
        public void onTouch() {
            proceed();
        }
    };

    private ViewPager.OnPageChangeListener pagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            pageIndicator.selectPage(position);
            btnNext.setButtonText(position < 2 ? R.string.btn_next : R.string.btn_finish);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void proceed() {

        SharedPreferences prefs = MyApplication.getInstance().getPrefs();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Prefs.SKIP_WALKTHROUGH, true);
        editor.apply();

        ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(this, 0, android.R.anim.fade_out);
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent, options.toBundle());

        ActivityCompat.finishAfterTransition(this);
    }
}
