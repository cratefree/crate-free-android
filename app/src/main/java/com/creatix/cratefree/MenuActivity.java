package com.creatix.cratefree;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.creatix.cratefree.api.calls.PushTokenRemoveCall;
import com.creatix.cratefree.gcm.RegistrationIntentService;
import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnTaskCompleteListener;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class MenuActivity extends BaseActivity {

    private AppCompatCheckBox checkNotifications;

    private ProgressDialog progress;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private boolean apiCallInProgress = false;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_menu;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                toggleProgress();
                startService(new Intent(MenuActivity.this, LocationService.class));
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Prefs.SENT_TOKEN_TO_SERVER));

        initViews();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void initViews() {

        progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.progress_sending));
        progress.setCancelable(false);

        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btn_contact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(MenuActivity.this, R.anim.slide_in_right, R.anim.slide_out_left);
                Intent intent = new Intent(MenuActivity.this, ContactActivity.class);
                MenuActivity.this.startActivity(intent, options.toBundle());
            }
        });

        findViewById(R.id.btn_about_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(MenuActivity.this, R.anim.slide_in_right, R.anim.slide_out_left);
                Intent intent = new Intent(MenuActivity.this, AboutActivity.class);
                MenuActivity.this.startActivity(intent, options.toBundle());
            }
        });

        checkNotifications = (AppCompatCheckBox) findViewById(R.id.chk_notifications);
        setNotificationsCheckbox();
        checkNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkNotifications.isChecked()) {
                    apiCallInProgress = true;
                    toggleProgress();

                    Intent intent = new Intent(MenuActivity.this, RegistrationIntentService.class);
                    intent.putExtra("PROGRESS", true);
                    startService(intent);
                } else {

                    PushTokenRemoveCall apiCall = new PushTokenRemoveCall(MenuActivity.this);
                    apiCall.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        TextView privacyPolicyText = findViewById(R.id.privacyPolicyMenu);
        privacyPolicyText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setNotificationsCheckbox() {
        checkNotifications.setChecked(MyApplication.getInstance().getPrefs().getBoolean(Prefs.NOTIFICATIONS_ENABLED, true));
    }

    public void toggleProgress() {

        if (progress == null || !apiCallInProgress) return;

        if (!progress.isShowing()) {
            progress.show();
        } else {
            progress.cancel();
            apiCallInProgress = false;
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, android.R.anim.fade_out);
    }
}
