package com.creatix.cratefree.api.objects;

import android.text.TextUtils;

import com.creatix.cratefree.R;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Sample text
 * </p>
 *
 * @author <a href="mailto:jan@thinkcreatix.com">Jan Jancek</a>
 *         created on 27.01.2017
 */
public enum Category implements TypeInterface {

    FARM("FARMS", "farm"),
    RETAIL("RESTAURANTS", "retail");

    private String label;
    private String category;

    Category(String label, String category) {
        this.label = label;
        this.category = category;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.category;
    }

    private static final Map<String, Category> categories = new HashMap<>();

    static {
        for (Category category : values()) {
            categories.put(category.getValue(), category);
        }
    }

    public static Category findByCategory(String category) {
        return categories.get(category);
    }

    @Override
    public int getRes() {
        return getCategoryRes(getValue());
    }

    public static int getCategoryRes(String categoryString) {

        if (TextUtils.isEmpty(categoryString)) {
            return 0;
        }

        Category category;

        try {
            category = Category.findByCategory(categoryString);
        } catch (NullPointerException e) {
            return 0;
        }

        switch (category) {
            case FARM:
                return R.drawable.pin;
            case RETAIL:
                return R.drawable.pin_retail;
            default:
                return R.drawable.pin;
        }
    }
}
