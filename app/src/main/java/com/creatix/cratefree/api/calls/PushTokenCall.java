package com.creatix.cratefree.api.calls;

import android.app.Activity;
import android.content.Context;

import com.creatix.cratefree.R;
import com.creatix.cratefree.api.HttpRetriever;
import com.creatix.cratefree.utils.JsonUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jano on 25.11.2015.
 */
public class PushTokenCall extends BaseCall {

    public PushTokenCall(Context context, HashMap<String, Object> params) {
        super(context, params);

        url = ApiUrl.API_PUSH_TOKEN;
    }

    @Override
    protected Boolean executeCall() {

        boolean success = false;

        HttpRetriever httpRetriever = new HttpRetriever(HttpRetriever.HttpMethod.POST);
        httpRetriever.addHeader("Content-Type", "application/json");
        httpRetriever.setJsonBody(getJsonBody(params));

        JSONObject jsonObject = httpRetriever.makeApiCall(context, url);
        try {
            success = jsonObject != null &&
                    ((jsonObject.has("saved") && JsonUtil.getBoolean(jsonObject, "saved")) ||
                            (jsonObject.has("updated")) && JsonUtil.getBoolean(jsonObject, "updated"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return success;
    }

    private JSONObject getJsonBody(HashMap<String, Object> params) {

        JSONObject body = new JSONObject();

        for (Map.Entry<String, Object> entry : params.entrySet()) {
            try {
                body.put(entry.getKey(), entry.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return body;
    }

    @Override
    protected void setProgressDialog() {
    }

    @Override
    public void toggleProgress() {
    }
}
