package com.creatix.cratefree.api.objects;

import android.text.TextUtils;

import com.creatix.cratefree.R;

import java.util.HashMap;
import java.util.Map;

public enum Product implements TypeInterface {

    BEEF("BEEF", "beef"),
    BISON("BISON", "bison"),
    CHICKEN("CHICKEN", "chicken"),
    DAIRY("DAIRY", "dairy"),
    DUCK("DUCK", "duck"),
    EGGS("EGGS", "eggs"),
    GOAT("GOAT", "goat"),
    GOOSE("GOOSE", "goose"),
    LAMB("LAMB / SHEEP", "lamb / sheep"),
    PORK("PORK", "pork"),
    RABBIT("RABBIT", "rabbit"),
    TURKEY("TURKEY", "turkey"),
    VEAL("VEAL", "veal");

    private String label;
    private String product;

    Product(String label, String product) {
        this.label = label;
        this.product = product;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.product;
    }

    private static final Map<String, Product> products = new HashMap<>();

    static {
        for (Product channel : values()) {
            products.put(channel.getValue(), channel);
        }
    }

    public static Product findByProduct(String product) {
        return products.get(product);
    }

    @Override
    public int getRes() {
        return getProductRes(getValue());
    }

    public static int getProductRes(String productString) {

        if (TextUtils.isEmpty(productString) || productString.equals("llamas")) {
            return 0;
        }

        Product product;

        try {
            product = Product.findByProduct(productString);
        } catch (NullPointerException e) {
            return 0;
        }

        switch (product) {
            case PORK:
                return R.drawable.filters_pig;
            case CHICKEN:
                return R.drawable.filters_chicken;
            case BEEF:
                return R.drawable.filters_beef;
            case VEAL:
                return R.drawable.filters_veal;
            case LAMB:
                return R.drawable.filters_lamb;
            case GOAT:
                return R.drawable.filters_goat;
            case TURKEY:
                return R.drawable.filters_turkey;
            case DUCK:
                return R.drawable.filters_duck;
            case BISON:
                return R.drawable.filters_bison;
            case RABBIT:
                return R.drawable.filters_rabbit;
            case GOOSE:
                return R.drawable.filters_goose;
            case DAIRY:
                return R.drawable.filters_dairy;
            case EGGS:
                return R.drawable.filters_egg;
            default:
                return R.drawable.beef;
        }
    }
}