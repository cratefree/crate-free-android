package com.creatix.cratefree.api.objects;

/**
 * <p>
 * Sample text
 * </p>
 *
 * @author <a href="mailto:jan@thinkcreatix.com">Jan Jancek</a>
 *         created on 08.02.2017
 */
public interface TypeInterface {

    String getLabel();
    String getValue();
    int getRes();
}
