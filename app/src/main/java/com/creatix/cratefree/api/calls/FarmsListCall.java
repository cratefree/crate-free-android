package com.creatix.cratefree.api.calls;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import com.creatix.cratefree.MyApplication;
import com.creatix.cratefree.api.HttpRetriever;
import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.db.factory.FarmDbFactory;
import com.creatix.cratefree.utils.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by jano on 25.11.2015.
 */
public class FarmsListCall extends BaseCall {

    public FarmsListCall(Activity activity) {
        super(activity);

        url = ApiUrl.API_PLACES_LIST;
    }

    @Override
    protected List<Farm> executeCall() {

        JSONObject jsonObject = new HttpRetriever(HttpRetriever.HttpMethod.GET).makeApiCall(activity, url);

        List<Farm> farms = null;
        if (jsonObject != null) {
            FarmDbFactory farmDbFactory = null;
            try {
                farmDbFactory = new FarmDbFactory(activity);
            } catch (PackageManager.NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            farmDbFactory.open();
            try {
                farms = farmDbFactory.setFarms(jsonObject.getJSONArray("places"));
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                farmDbFactory.close();
            }
        }

        if (farms != null && farms.size() > 0) {
            SharedPreferences prefs = MyApplication.getInstance().getPrefs();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong(Prefs.LAST_UPDATED, System.currentTimeMillis());
            editor.apply();
        }

        return farms;
    }


    @Override
    protected void setProgressDialog() {
    }

    @Override
    public void toggleProgress() {
    }
}
