package com.creatix.cratefree.api.calls;

/**
 * Created by jano on 25.11.2015.
 */
public class ApiUrl {

    public static final String API_VERISON = "?v=2";

    public static final String API_PLACES_LIST = "/places";
    public static final String API_FARMS_LIST = "/farms" + API_VERISON;
    public static final String API_SEND = "/message/send" + API_VERISON;
    public static final String API_PUSH_TOKEN = "/pushToken" + API_VERISON;
}
