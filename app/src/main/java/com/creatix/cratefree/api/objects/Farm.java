package com.creatix.cratefree.api.objects;

import android.content.ContentValues;
import android.content.res.Resources;
import android.text.TextUtils;

import com.creatix.cratefree.R;
import com.creatix.cratefree.db.tables.FarmTable;
import com.creatix.cratefree.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.creatix.cratefree.utils.Constants.DELIMITER;

/**
 * Created by jano on 26.11.2015.
 */
public class Farm implements Serializable {

    private Long id;
    private String farmId;
    private String name;
    private String brand;
    private String v;
    private String URL;
    private String phone;
    private String email;
    private String person;
    private String twitter;
    private String instagram;
    private String facebook;
    private String longitude;
    private String latitude;
    private String county;
    private String zip;
    private String town;
    private String street;
    private Float distance;
    private Category category;
    private List<String> products;
    private List<String> types;

    public Long getId() {
        return id;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {

        ArrayList<String> arrayList = null;

        if (products != null && products.contains("llamas")) {
            arrayList = new ArrayList<>(products);
            arrayList.remove("llamas");
        }

        this.products = arrayList != null ? arrayList : products;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Float getDistance() {
        return distance;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean hasSocial() {
        return !TextUtils.isEmpty(this.twitter) ||
                !TextUtils.isEmpty(this.facebook) ||
                !TextUtils.isEmpty(this.instagram);
    }

    public String getAddressList() {

        String delimiter = ", ";

        StringBuilder stringBuilder = new StringBuilder();

        if (!TextUtils.isEmpty(street)) {
            stringBuilder.append(street);
        }

        if (!TextUtils.isEmpty(county)) {
            if (stringBuilder.toString().length() != 0) {
                stringBuilder.append(delimiter);
            }
            stringBuilder.append(county);
        }

        if (!TextUtils.isEmpty(zip)) {
            if (stringBuilder.toString().length() != 0) {
                stringBuilder.append(delimiter);
            }
            stringBuilder.append(zip);
        }

        if (!TextUtils.isEmpty(town)) {
            if (stringBuilder.toString().length() != 0) {
                stringBuilder.append(delimiter);
            }
            stringBuilder.append(town);
        }

        return stringBuilder.toString();
    }

    public String getAddress() {

        String delimiter = ", ";

        StringBuilder stringBuilder = new StringBuilder();

        if (!TextUtils.isEmpty(town)) {
            stringBuilder.append(town);
        }

        if (!TextUtils.isEmpty(zip)) {
            if (stringBuilder.toString().length() != 0) {
                stringBuilder.append(delimiter);
            }
            stringBuilder.append(zip);
        }

        if (!TextUtils.isEmpty(county)) {
            if (stringBuilder.toString().length() != 0) {
                stringBuilder.append(delimiter);
            }
            stringBuilder.append(county);
        }

        return stringBuilder.toString();
    }

    public String getTypesString() {

        String delimiter = " & ";

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < types.size(); i++) {
            String type = types.get(i);
            if (type.length() > 0) {
                stringBuilder.append(type.substring(0, 1).toUpperCase());
            }
            if (type.length() > 1) {
                stringBuilder.append(type.substring(1));
            }
            if (i < types.size() - 1) {
                stringBuilder.append(delimiter);
            }
        }

        return stringBuilder.toString();
    }

    public String getProductsString(Resources res) {

        String delimiter = res.getString(R.string.char_bullet);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < products.size(); i++) {
            stringBuilder.append(products.get(i).toUpperCase());
            if (i < products.size() - 1) {
                stringBuilder.append(delimiter);
            }
        }

        return stringBuilder.toString();
    }

    public String getDistanceInMiles() {

        String miles = "";
        if (this.distance != null) {
            double raw = this.distance / 1609.344d;
            miles = Utils.round(raw, raw > 10d ? 0 : 1);
        }

        return miles;
    }

    public boolean hashChanged(ContentValues values) {

        String name = values.getAsString(FarmTable.NAME);
        String brand = values.getAsString(FarmTable.BRAND);
        String v = values.getAsString(FarmTable.V);
        String URL = values.getAsString(FarmTable.URL);
        String phone = values.getAsString(FarmTable.PHONE);
        String email = values.getAsString(FarmTable.EMAIL);
        String person = values.getAsString(FarmTable.PERSON);
        String longitude = values.getAsString(FarmTable.LONGITUDE);
        String latitude = values.getAsString(FarmTable.LATITUDE);
        String county = values.getAsString(FarmTable.COUNTY);
        String zip = values.getAsString(FarmTable.ZIP);
        String town = values.getAsString(FarmTable.TOWN);
        String street = values.getAsString(FarmTable.STREET);
        String category = values.getAsString(FarmTable.CATEGORY);
        String products = values.getAsString(FarmTable.PRODUCTS);
        String types = values.getAsString(FarmTable.TYPES);

        if (name != null && !name.equals(this.name)) {
            return true;
        } else if (brand != null && !brand.equals(this.brand)) {
            return true;
        } else if (v != null && !v.equals(this.v)) {
            return true;
        } else if (URL != null && !URL.equals(this.URL)) {
            return true;
        } else if (phone != null && !phone.equals(this.phone)) {
            return true;
        } else if (email != null && !email.equals(this.email)) {
            return true;
        } else if (person != null && !person.equals(this.person)) {
            return true;
        } else if (longitude != null && !longitude.equals(this.longitude)) {
            return true;
        } else if (latitude != null && !latitude.equals(this.latitude)) {
            return true;
        } else if (county != null && !county.equals(this.county)) {
            return true;
        } else if (zip != null && !zip.equals(this.zip)) {
            return true;
        } else if (town != null && !town.equals(this.town)) {
            return true;
        } else if (street != null && !street.equals(this.street)) {
            return true;
        } else if (products != null && !products.equals(stringifyArrayList(this.products))) {
            return true;
        } else if (types != null && !types.equals(stringifyArrayList(this.types))) {
            return true;
        } else if ((this.category == null && category != null) || (category !=null && !category.equals(this.category.getLabel()))) {
            return true;
        }

        return false;
    }

    private String stringifyArrayList(List<String> arrayList) {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < arrayList.size(); i++) {
            stringBuilder.append(arrayList.get(i));
            if (i < arrayList.size() - 1) {
                stringBuilder.append(DELIMITER);
            }
        }

        return stringBuilder.toString();
    }
}
