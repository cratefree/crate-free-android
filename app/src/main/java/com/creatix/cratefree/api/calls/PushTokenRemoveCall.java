package com.creatix.cratefree.api.calls;

import android.app.Activity;
import android.content.Intent;
import android.location.LocationListener;

import com.creatix.cratefree.LocationService;
import com.creatix.cratefree.MyApplication;
import com.creatix.cratefree.R;
import com.creatix.cratefree.utils.Prefs;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by jano on 25.11.2015.
 */
public class PushTokenRemoveCall extends BaseCall {

    public PushTokenRemoveCall(Activity activity) {
        super(activity);
    }

    @Override
    protected Boolean executeCall() {

        InstanceID instanceID = InstanceID.getInstance(activity);
        try {
            instanceID.deleteToken(activity.getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE);
            MyApplication.getInstance().getPrefs().edit().remove(Prefs.GCM_TOKEN).putBoolean(Prefs.NOTIFICATIONS_ENABLED, false).apply();

            activity.stopService(new Intent(activity, LocationService.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    protected void setProgressDialog() {
    }

    @Override
    public void toggleProgress() {
    }
}
