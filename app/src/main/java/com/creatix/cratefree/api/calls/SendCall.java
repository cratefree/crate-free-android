package com.creatix.cratefree.api.calls;

import android.app.Activity;

import com.creatix.cratefree.R;
import com.creatix.cratefree.api.HttpRetriever;
import com.creatix.cratefree.utils.JsonUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jano on 25.11.2015.
 */
public class SendCall extends BaseCall {

    public SendCall(Activity activity, HashMap<String, Object> params) {
        super(activity, params);

        url = ApiUrl.API_SEND;
    }

    @Override
    protected Boolean executeCall() {

        boolean success = false;

        HttpRetriever httpRetriever = new HttpRetriever(HttpRetriever.HttpMethod.POST);
        httpRetriever.addHeader("Content-Type", "application/json");
        httpRetriever.setJsonBody(getJsonBody(params));

        JSONObject jsonObject = httpRetriever.makeApiCall(activity, url);
        if (jsonObject != null && jsonObject.has("sent")) {
            try {
                success = JsonUtil.getBoolean(jsonObject, "sent");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return success;
    }

    private JSONObject getJsonBody(HashMap<String, Object> params) {

        JSONObject jsonParams = new JSONObject();

        for (Map.Entry<String, Object> entry : params.entrySet()) {
            try {
                jsonParams.put(entry.getKey(), entry.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject body = new JSONObject();
        try {
            body.put("message", jsonParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return body;
    }

    @Override
    protected void setProgressDialog() {
        progress.setMessage(res.getString(R.string.progress_loading));
    }
}
