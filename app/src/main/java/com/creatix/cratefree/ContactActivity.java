package com.creatix.cratefree;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creatix.cratefree.api.calls.SendCall;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnTaskCompleteListener;
import com.creatix.cratefree.views.ContactFarmLayout;
import com.creatix.cratefree.views.ContactOtherLayout;
import com.creatix.cratefree.views.ContactVolunteerLayout;
import com.creatix.cratefree.views.lists.adapters.FormLayout;

public class ContactActivity extends BaseActivity {

    private FrameLayout formHolder;

    private TextView btn_contact_reason;

    private FormLayout suggestFarmLayout;
    private FormLayout volunteerLayout;
    private FormLayout otherLayout;

    private AlertDialog dialog;

    private static final int OTHER = 1;
    private static final int VOLUNTEER = 2;
    private static final int FARM = 3;

    private int currentForm;
    private FormLayout currentFormLayout;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        initViews();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new AlertDialog.Builder(this).setView(LinearLayout.inflate(this, R.layout.dialog_contact_reason, null)).create();

        formHolder = (FrameLayout) findViewById(R.id.form_holder);

        suggestFarmLayout = new ContactFarmLayout(this);
        volunteerLayout = new ContactVolunteerLayout(this);
        otherLayout = new ContactOtherLayout(this);

        btn_contact_reason = (TextView) findViewById(R.id.btn_contact_reason);

        setForm(FARM);

    }

    private void initViews() {

        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_contact_reason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showContactReasonDialog();
            }
        });

        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFormLayout.isValid()) {
                    SendCall sendCall = new SendCall(ContactActivity.this, currentFormLayout.getParams());
                    sendCall.execute();
                    sendCall.setOnTaskCompleteListener(new OnTaskCompleteListener() {
                        @Override
                        public void OnTaskComplete(Object result) {
                            Utils.showToast(ContactActivity.this, getString(R.string.toast_send));
                            finish();
                        }
                    });
                }
            }
        });
    }

    private void showContactReasonDialog() {

        dialog.show();

        dialog.findViewById(R.id.btn_suggest_farm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setForm(FARM);
            }
        });
        dialog.findViewById(R.id.btn_volunteer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setForm(VOLUNTEER);
            }
        });
        dialog.findViewById(R.id.btn_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setForm(OTHER);
            }
        });
    }

    private void setForm(int type) {

        if (type != currentForm || formHolder.getChildCount() == 0) {
            currentForm = type;
            formHolder.removeAllViews();

            switch (type) {
                case OTHER:
                    currentFormLayout = otherLayout;
                    formHolder.addView((ViewGroup) otherLayout);
                    btn_contact_reason.setText(getString(R.string.btn_other));
                    break;
                case VOLUNTEER:
                    currentFormLayout = volunteerLayout;
                    formHolder.addView((ViewGroup) volunteerLayout);
                    btn_contact_reason.setText(getString(R.string.btn_volunteer));
                    break;
                case FARM:
                    currentFormLayout = suggestFarmLayout;
                    formHolder.addView((ViewGroup) suggestFarmLayout);
                    btn_contact_reason.setText(getString(R.string.btn_suggest_farms));
                    break;
            }
        }

        dialog.cancel();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
