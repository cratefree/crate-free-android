package com.creatix.cratefree;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.creatix.cratefree.utils.Prefs;
import com.google.android.gms.gcm.GcmListenerService;

public class GCMIntentService extends GcmListenerService {

    public static int NOTIFICATION_ID = 1;
    public static final String TAG = "GCM";
    private NotificationManager mNotificationManager;

    @Override
    public void onMessageReceived(String from, Bundle data) {

        if (data != null && MyApplication.getInstance().getPrefs().getBoolean(Prefs.NOTIFICATIONS_ENABLED, true)) {
            sendNotification(data);
        }
    }

    private void sendNotification(Bundle extras) {

        String body = extras.getString("gcm.notification.body");

        if (body != null) {

            mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

            Intent i = new Intent(this, HomeActivity.class);
            i.putExtras(extras);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.icon))
                    .setLights(Color.GREEN, 500, 500)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(getString(R.string.app_name)).bigText(body))
                    .setContentIntent(contentIntent)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(body)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            mNotificationManager.notify(NOTIFICATION_ID++, mBuilder.build());
        }
    }
}