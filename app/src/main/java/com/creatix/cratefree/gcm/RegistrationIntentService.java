package com.creatix.cratefree.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.creatix.cratefree.MyApplication;
import com.creatix.cratefree.R;
import com.creatix.cratefree.api.calls.BaseCall;
import com.creatix.cratefree.api.calls.PushTokenCall;
import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnTaskCompleteListener;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.HashMap;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    private SharedPreferences sharedPreferences;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sharedPreferences = MyApplication.getInstance().getPrefs();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Log.i(TAG, "GCM Registration Token: " + token);

            // TODO: Implement this method to send any registration to your app's servers.
            sendRegistrationToServer(token);

            // Subscribe to topic channels
            subscribeTopics(token);

        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            sharedPreferences.edit().putBoolean(Prefs.SENT_TOKEN_TO_SERVER, false).apply();
        }
    }

    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("token", token);
        params.put("type", "a");
        params.put("dev", Utils.DEV_BUILD);
        params.put("userId", Utils.getUniqueId());

        BaseCall apiCall = new PushTokenCall(this, params);
        apiCall.setOnTaskCompleteListener(new OnTaskCompleteListener() {
            @Override
            public void OnTaskComplete(Object result) {

                boolean success = (boolean) result;
                sharedPreferences.edit().putBoolean(Prefs.SENT_TOKEN_TO_SERVER, success).apply();
                if (success) {
                    sharedPreferences.edit().putString(Prefs.GCM_TOKEN, token).putBoolean(Prefs.NOTIFICATIONS_ENABLED, true).apply();
                }

                // Notify UI that registration has completed, so the progress indicator can be hidden.
                Intent registrationComplete = new Intent(Prefs.SENT_TOKEN_TO_SERVER);
                LocalBroadcastManager.getInstance(RegistrationIntentService.this).sendBroadcast(registrationComplete);
            }
        });
        apiCall.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}