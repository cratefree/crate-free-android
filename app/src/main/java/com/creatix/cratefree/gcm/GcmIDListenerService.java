package com.creatix.cratefree.gcm;

import android.content.Intent;

import com.creatix.cratefree.R;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;

import java.io.IOException;

public class GcmIDListenerService extends InstanceIDListenerService {

    private static final String TAG = "GCM_INSTANCE_ID";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        startService(new Intent(this, RegistrationIntentService.class));
    }
}