package com.creatix.cratefree;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.db.factory.FarmDbFactory;
import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("ResourceType")
public class LocationService extends Service implements LocationListener {

    private LocationManager locationManager;

    private FarmDbFactory farmDbFactory;

    private SharedPreferences prefs;

    private NotificationManager mNotificationManager;

    public static int NOTIFICATION_ID = 1000;

    @Override
    public void onCreate() {
        super.onCreate();

        prefs = MyApplication.getInstance().getPrefs();

        try {
            farmDbFactory = new FarmDbFactory(this);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (hasPermission()) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 300000, 1500, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            Utils.appendLog("LOCATION_SERVICE", location.getProvider() + "   ACCURACY:  " + location.getAccuracy() +
                    "  LAT:  " + location.getLatitude() + ",  LON:  " + location.getLongitude());

            List<Farm> farms = getAllNearbyFarms(location);
            for (Farm farm : farms) {
                long timestamp = prefs.getLong(farm.getId().toString(), 0L);
                if (timestamp == 0L || hiddenElapsed(timestamp)) {
                    sendNotification(farm);
                    prefs.edit().putLong(farm.getId().toString(), System.currentTimeMillis()).apply();
                    break;
                }
            }
        }
    }

    private void sendNotification(Farm farm) {

        if (!MyApplication.getInstance().getPrefs().getBoolean(Prefs.NOTIFICATIONS_ENABLED, true)) {
            return;
        }

        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent i = new Intent(this, HomeActivity.class);
        i.putExtra("farm", farm);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.icon)
                .setLights(Color.GREEN, 500, 500)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(getString(R.string.app_name)).bigText(String.format(getString(R.string.farm_nearby), farm.getName())))
                .setContentIntent(contentIntent)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(String.format(getString(R.string.farm_nearby), farm.getName()))
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private boolean hiddenElapsed(long timestamp) {
        return System.currentTimeMillis() - 864000000l > timestamp;
    }

    private List<Farm> getAllNearbyFarms(Location location) {

        List<Farm> nearbyFarms = new ArrayList<>();

        farmDbFactory.open();
        try {
            List<Farm> farms = farmDbFactory.getAllFarmsWithData();
            for (Farm farm : farms) {
                if (farm.getLatitude() != null && farm.getLongitude() != null) {
                    Location loc = new Location("");
                    loc.setLatitude(Double.parseDouble(farm.getLatitude()));
                    loc.setLongitude(Double.parseDouble(farm.getLongitude()));

                    double distance = location.distanceTo(loc) / 1609.344d;
                    farm.setDistance((float) distance);
                    if (distance < 8) {
                        nearbyFarms.add(farm);
                    }
                }
            }
        } finally {
            farmDbFactory.close();
        }

        Collections.sort(nearbyFarms, new Comparator<Farm>() {
            public int compare(Farm u1, Farm u2) {
                return u1.getDistance().compareTo(u2.getDistance());
            }
        });

        return nearbyFarms;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (hasPermission()) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }
}


