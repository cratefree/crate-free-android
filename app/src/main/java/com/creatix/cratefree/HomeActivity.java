package com.creatix.cratefree;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.MapView;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.OnMapReadyCallback;
import com.balysv.materialmenu.MaterialMenuDrawable.IconState;
import com.balysv.materialmenu.MaterialMenuView;
import com.creatix.cratefree.api.objects.Category;
import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.api.objects.Product;
import com.creatix.cratefree.db.factory.FarmDbFactory;
import com.creatix.cratefree.utils.AnimUtils;
import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.Utils;
import com.creatix.cratefree.utils.listeners.OnBackPressedListener;
import com.creatix.cratefree.utils.listeners.OnFiltersSelectedListener;
import com.creatix.cratefree.utils.listeners.OnItemClickListener;
import com.creatix.cratefree.views.CreatixButton;
import com.creatix.cratefree.views.dialog.FiltersDialog;
import com.creatix.cratefree.views.lists.adapters.FarmListAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.balysv.materialmenu.MaterialMenuDrawable.IconState.ARROW;
import static com.balysv.materialmenu.MaterialMenuDrawable.IconState.BURGER;

@SuppressWarnings("ResourceType")
public class HomeActivity extends BaseActivity implements OnMapReadyCallback, LocationListener, OnFiltersSelectedListener {

    private GoogleMap mMap;
    private RecyclerView farmRecyclerView;
    private FarmListAdapter farmListAdapter;

    private Toolbar toolbar;
    private ActionBar actionBar;
    private AppBarLayout appbar;

    private FarmDbFactory farmDbFactory;
    private RelativeLayout searchBar;
    private RelativeLayout detaillLayout;
    private CreatixButton btnDirections;
    private CreatixButton btnPosition;

    private EditText searchEditText;
    private EditText fakeEditText;
    private MaterialMenuView menuView;
    private Map<Long, Marker> markers = new HashMap<>();
    private LatLng IL = new LatLng(40.5269949d, -89.0222352d);

    private View listShadow;
    private View toolbarShadow;

    private LocationManager locationManager;
    private String provider;
    private Marker myLocation;

    private List<Farm> mFarms;

    private List<Product> selectedProducts;

    private List<Category> selectedCategories;

    private Criteria criteria;

    private BitmapDescriptor farmIcon;

    private BitmapDescriptor retailIcon;

    private MapView mapView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapsInitializer.initialize(HomeActivity.this);
        farmIcon = BitmapDescriptorFactory.fromResource(R.drawable.pin);
        retailIcon = BitmapDescriptorFactory.fromResource(R.drawable.pin_retail);

        try {
            farmDbFactory = new FarmDbFactory(this);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        initMap();
        initViews();

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.get("farm") != null) {
            Farm farm = (Farm) extras.get("farm");
            showDetailActivity(farm);
        }

        showNotificationDialog(extras);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_map;
    }

    private void initMap() {

        LayoutInflater.from(HomeActivity.this).inflate(R.layout.layout_map, (FrameLayout) findViewById(R.id.map_holder));

        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(new Bundle());
        mapView.getExtendedMapAsync(this);
    }

    protected void initViews() {

        farmRecyclerView = (RecyclerView) findViewById(R.id.farm_list);
        farmListAdapter = new FarmListAdapter(new ArrayList<Farm>());
        farmRecyclerView.setAdapter(farmListAdapter);
        farmRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchEditText = (EditText) findViewById(R.id.edittext_search);
        fakeEditText = (EditText) findViewById(R.id.fake_edittext);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        menuView = (MaterialMenuView) findViewById(R.id.btn_menu);
        detaillLayout = (RelativeLayout) findViewById(R.id.farm_detail);
        btnDirections = (CreatixButton) findViewById(R.id.btn_directions);
        btnPosition = (CreatixButton) findViewById(R.id.btn_position);

        listShadow = findViewById(R.id.list_shadow);
        toolbarShadow = findViewById(R.id.toolbar_shadow);

        searchBar = (RelativeLayout) findViewById(R.id.search_bar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        setAppBar();
        loadData();
    }

    private void setAppBar() {
        float heightDp = Utils.getRemaingingScreenSpace(this, 110);
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        lp.height = (int) heightDp;

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, final int verticalOffset) {
                if (verticalOffset != 0) {

                    final double ratio = (double) Utils.getScreenHeight(HomeActivity.this) / Math.abs(verticalOffset);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.search_bar).setBackgroundResource(ratio < 1.6d ? 0 : R.drawable.search_bg);
                            menuView.animateState(ratio < 1.6d ? ARROW : BURGER);
                        }
                    }, 200);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        if (ratio < 1.6d && toolbarShadow.getVisibility() == View.GONE) {
                            AnimUtils.toggleFade(HomeActivity.this, toolbarShadow, true);
                        } else if (ratio > 1.6d && toolbarShadow.getVisibility() == View.VISIBLE) {
                            AnimUtils.toggleFade(HomeActivity.this, toolbarShadow, false);
                        }
                    }

                    if (ratio < 1.6d && listShadow.getVisibility() == View.VISIBLE) {
                        AnimUtils.toggleFade(HomeActivity.this, listShadow, false);
                    } else if (ratio > 1.6d && listShadow.getVisibility() == View.GONE) {
                        AnimUtils.toggleFade(HomeActivity.this, listShadow, true);
                    }

                    if (ratio < 2d && btnPosition.getVisibility() == View.VISIBLE) {
                        AnimUtils.toggleGrowAnim(HomeActivity.this, btnPosition, false);
                    } else if (ratio > 2d && btnPosition.getVisibility() == View.GONE) {
                        AnimUtils.toggleGrowAnim(HomeActivity.this, btnPosition, true);
                    }
                } else {
//                    Utils.hideKeyboard(activity, searchEditText);
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return false;
            }
        });
        params.setBehavior(behavior);
    }

    private void setEventHandlers() {
        farmListAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(Object object) {

                Farm farm = (Farm) object;

                Marker marker = markers.get(farm.getId());
                if (marker != null) {
                    selectFarm(marker, farm.getCategory());
                }
                setDetailLayout(farm);
                Utils.hideKeyboard(HomeActivity.this, searchEditText);
                fakeEditText.requestFocus();
            }
        });

        findViewById(R.id.btn_filters).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FiltersDialog(HomeActivity.this, HomeActivity.this, selectedProducts, selectedCategories).show();
            }
        });

        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                appbar.setExpanded(!hasFocus, true);
                if (hasFocus) {
                    resetMapAndDetail();
                }
            }
        });

        btnPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myLocation != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation.getPosition(), 10f));
                } else {
                    Utils.showToast(HomeActivity.this, R.string.toast_localizing);
                }
            }
        });

        menuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (menuView.getState()) {
                    case ARROW:
                        resetMapAndDetail();
                        appbar.setExpanded(true);
                        Utils.hideKeyboard(HomeActivity.this, searchEditText);
                        break;
                    case BURGER:
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(HomeActivity.this, android.R.anim.fade_in, android.R.anim.fade_out);
                        Intent intent = new Intent(HomeActivity.this, MenuActivity.class);
                        HomeActivity.this.startActivity(intent, options.toBundle());
                        break;
                }
            }
        });

        setOnBackPressedListener(new OnBackPressedListener() {
            @Override
            public void onBackPressed() {
                if (menuView.getState() == ARROW) {
                    resetMapAndDetail();
                    appbar.setExpanded(true);
                    Utils.hideKeyboard(HomeActivity.this, searchEditText);
                } else {
                    HomeActivity.this.finish();
                }
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (farmListAdapter != null && farmRecyclerView != null)
                    farmListAdapter.getFilter().filter(s);
            }
        });
    }

    private Marker previousMarker;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(IL, 6.0f));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker != myLocation) {
                    Farm farm = marker.getData();
                    selectFarm(marker, farm.getCategory());
                    setDetailLayout(farm);
                }

                return true;
            }
        });
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng position) {
                resetMapAndDetail();
            }
        });

        if (myLocation == null && hasPermission()) {
            addMyLocation(locationManager.getLastKnownLocation(provider));
        } else {
            this.finish();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        addMyLocation(location);

        if (mFarms != null) {
            farmListAdapter.setFarms(sortFarms(location, mFarms));
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void addMyLocation(Location location) {
        if (location != null && mMap != null) {
            if (myLocation != null) {
                myLocation.remove();
            }
            myLocation = mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_selected)));

        }
    }

    @Override
    public void OnFiltersSelected(List<Product> products, List<Category> categories) {

        if (products != null) {
            selectedProducts = products;
            loadData();
        }

        if (categories != null) {
            selectedCategories = categories;
            loadData();
        }
    }

    private void loadData() {
        new FarmsListTask(selectedProducts, selectedCategories).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class FarmsListTask extends AsyncTask<Void, Void, List<Farm>> {

        private List<Product> products;
        private List<Category> categories;

        public FarmsListTask(List<Product> products, List<Category> categories) {
            this.products = products;
            this.categories = categories;
        }

        @Override
        protected List<Farm> doInBackground(Void... params) {
            farmDbFactory.open();
            try {
                if ((products == null || products.isEmpty()) &&
                        (categories == null || categories.isEmpty())) {
                    return farmDbFactory.getAllFarmsWithData();
                } else {
                    return farmDbFactory.getFilteredFarms(products, categories);
                }
            } finally {
                farmDbFactory.close();
            }
        }

        @Override
        protected void onPostExecute(List<Farm> farms) {
            super.onPostExecute(farms);

            if (farms != null) {
                HomeActivity.this.mFarms = farms;
                setFarms(farms);
            }
        }
    }

    private void setFarms(List<Farm> farms) {

        for (Marker marker : mMap.getMarkers()) {
            if (!marker.equals(myLocation)) {
                marker.remove();
            }
        }

        markers.clear();

        for (Farm farm : farms) {
            if (!TextUtils.isEmpty(farm.getLatitude()) && !TextUtils.isEmpty(farm.getLongitude())) {
                Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(farm.getLatitude()), Double.parseDouble(farm.getLongitude()))).title(farm.getName()));
                marker.setData(farm);
                marker.setIcon(Category.FARM.equals(farm.getCategory()) ? farmIcon : retailIcon);
                markers.put(farm.getId(), marker);
            }
        }

        farmListAdapter.setFarms(sortFarms(locationManager.getLastKnownLocation(provider), farms));
    }

    private void selectFarm(Marker marker, Category category) {

        resetPreviousMarker();

        previousMarker = marker;

        marker.setIcon(BitmapDescriptorFactory.fromResource(Category.FARM.equals(category) ? R.drawable.pin_selected_2x : R.drawable.pin_selected_retail));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 10f));
        appbar.setExpanded(true);
    }

    private void setDetailLayout(final Farm farm) {

        AnimUtils.toggleSlideBottom(this, detaillLayout, true);
        if (!TextUtils.isEmpty(farm.getLatitude()) && !TextUtils.isEmpty(farm.getLongitude())) {

            AnimUtils.toggleGrowAnim(this, btnDirections, true);
            btnDirections.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.navigateTo(HomeActivity.this, farm.getLatitude(), farm.getLongitude());
                }
            });
        }

        TextView titleTextView = (TextView) detaillLayout.findViewById(R.id.title);
        TextView subtitleTextView = (TextView) detaillLayout.findViewById(R.id.subtitle);
        TextView distanceTextView = (TextView) detaillLayout.findViewById(R.id.distance);
        LinearLayout productsLayout = (LinearLayout) detaillLayout.findViewById(R.id.layout_products);

        titleTextView.setText(farm.getName().toUpperCase());
        subtitleTextView.setText(farm.getAddressList());
        if (!TextUtils.isEmpty(farm.getDistanceInMiles())) {
            distanceTextView.setText(String.format(getString(R.string.distance_list), farm.getDistanceInMiles()));
        }

        productsLayout.removeAllViews();

        for (String product : farm.getProducts()) {
            ImageView productImage = new ImageView(this);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(Utils.getPixels(this, 25), ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, Utils.getPixels(this, 3), 0);
            lp.gravity = Gravity.BOTTOM;
            productImage.setLayoutParams(lp);
            productImage.setImageResource(Product.getProductRes(product));
            productImage.setAlpha(0.8f);
            productsLayout.addView(productImage);
        }
        menuView.animateState(ARROW);

        detaillLayout.findViewById(R.id.btn_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDetailActivity(farm);
            }
        });
    }

    private void showDetailActivity(Farm farm) {
        if (farm == null) {
            Toast.makeText(this, R.string.cannot_show_farm,Toast.LENGTH_LONG).show();
            return;
        }
        ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(HomeActivity.this, R.anim.slide_in_right, R.anim.slide_out_left);
        Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
        intent.putExtra("farm", farm);
        HomeActivity.this.startActivity(intent, options.toBundle());
    }

    private void resetMapAndDetail() {

        if (detaillLayout.getVisibility() == View.VISIBLE) {

            menuView.animateState(IconState.BURGER);

            AnimUtils.toggleSlideBottom(this, detaillLayout, false);
            AnimUtils.toggleGrowAnim(this, btnDirections, false);

            resetPreviousMarker();
        }
    }

    private void resetPreviousMarker() {

        try {
            if (previousMarker != null) {

                Farm farm = previousMarker.getData();

                previousMarker.setIcon(Category.FARM.equals(farm.getCategory()) ? BitmapDescriptorFactory.fromResource(R.drawable.pin) : BitmapDescriptorFactory.fromResource(R.drawable.pin_retail));
            }
        } catch (NullPointerException e) {
            // JUST IN CASE
        }
    }

    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private List<Farm> sortFarms(Location location, List<Farm> farms) {

        if (location != null) {

            for (Farm farm : farms) {
                if (farm.getLatitude() != null && farm.getLongitude() != null) {
                    Location loc = new Location("");
                    loc.setLatitude(Double.parseDouble(farm.getLatitude()));
                    loc.setLongitude(Double.parseDouble(farm.getLongitude()));
                    farm.setDistance(location.distanceTo(loc));
                } else {
                    farm.setDistance(10000000f);
                }
            }

            Collections.sort(farms, new Comparator<Farm>() {
                public int compare(Farm u1, Farm u2) {
                    return u1.getDistance().compareTo(u2.getDistance());
                }
            });
        } else {

            Collections.sort(farms, new Comparator<Farm>() {
                public int compare(Farm u1, Farm u2) {
                    return u1.getName().compareTo(u2.getName());
                }
            });

        }

        return farms;
    }

    private void showNotificationDialog(final Bundle bundle) {

        if (bundle != null) {

            final String url = bundle.getString("url");
            final String body = bundle.getString("gcm.notification.body");

            if (body != null || url != null) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                        .setNegativeButton(R.string.dialog_btn_dismiss, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                if (!TextUtils.isEmpty(body)) {
                    dialog.setMessage(body);
                }

                if (!TextUtils.isEmpty(url)) {
                    dialog.setPositiveButton(getString(R.string.dialog_btn_url), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String parsedUrl = (url.toLowerCase().contains("http://") || url.toLowerCase().contains("https://")) ? url : "http://" + url;
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(parsedUrl));
                            startActivity(browserIntent);
                        }
                    });
                }
                dialog.create().show();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (hasPermission()) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            provider = locationManager.getBestProvider(criteria, true);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 500, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 500, this);

            if (MyApplication.getInstance().getPrefs().getBoolean(Prefs.NOTIFICATIONS_ENABLED, true)) {
                startService(new Intent(this, LocationService.class));
            }
        } else {
            this.finish();
        }

        mapView.onResume();
        setEventHandlers();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle extras = intent.getExtras();
        if (extras != null && extras.get("farm") != null) {
            Farm farm = (Farm) extras.get("farm");
            showDetailActivity(farm);
        }

        showNotificationDialog(extras);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

        if (hasPermission()) {
            locationManager.removeUpdates(this);
        } else {
            this.finish();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
