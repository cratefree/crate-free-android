package com.creatix.cratefree;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;

//import com.crashlytics.android.Crashlytics;
import com.creatix.cratefree.db.SQLiteDatabaseHelper;
import com.creatix.cratefree.utils.Prefs;
import com.creatix.cratefree.utils.SecurePreferences;

//import io.fabric.sdk.android.Fabric;

/**
 * @author Jano
 */
public class MyApplication extends Application {

    private static MyApplication myApplication;
    private SharedPreferences prefs;

    public static MyApplication getInstance() {
        return myApplication;
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    /**
     * @return skip - first time user or mature?
     */
    public boolean skipWalktrough() {
        return prefs.getBoolean(Prefs.SKIP_WALKTHROUGH, false);
    }

    public void requestAppUpdate() {
        prefs.edit().putLong(Prefs.LAST_UPDATED, 0).apply();
    }

    public Long lastUpdated() {
        return prefs.getLong(Prefs.LAST_UPDATED, 0);
    }

    public void onCreate() {
        super.onCreate();
        MyApplication.myApplication = this;
        prefs = new SecurePreferences(this);
        //Fabric.with(this, new Crashlytics());
    }

    /////////////////////////////////////////////////////
    //////////////       CUSTOM LOGGER       ////////////
    /////////////////////////////////////////////////////

    @Override
    public void onTerminate() {
        super.onTerminate();

        try {
            SQLiteDatabaseHelper.getHelper(this).close();
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean updateApp() {

        long threshold = 43200000l;

        return lastUpdated() == 0 || (System.currentTimeMillis() - lastUpdated()) > threshold;
//        return true;
    }

}
