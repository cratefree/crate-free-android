package com.creatix.cratefree;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.creatix.cratefree.utils.Constants;

public class AboutActivity extends BaseActivity {

    private TextView whyThisAppText;
    private ImageButton btnWhyThisApp;

    private TextView aboutCrateFreeText;
    private ImageButton btnAboutCrateFree;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        whyThisAppText = (TextView) findViewById(R.id.text_why_this_app);
        aboutCrateFreeText = (TextView) findViewById(R.id.text_about_cratefree);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        initViews();
    }

    private void initViews() {

        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btn_contact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(AboutActivity.this, R.anim.slide_in_right, R.anim.slide_out_left);
                Intent intent = new Intent(AboutActivity.this, ContactActivity.class);
                AboutActivity.this.startActivity(intent, options.toBundle());
            }
        });

        btnWhyThisApp = (ImageButton) findViewById(R.id.btn_why_this_app);
        btnWhyThisApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expand(whyThisAppText, btnWhyThisApp);
            }
        });

        btnAboutCrateFree = (ImageButton) findViewById(R.id.btn_about_cratefree);
        btnAboutCrateFree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expand(aboutCrateFreeText, btnAboutCrateFree);
            }
        });

        findViewById(R.id.btn_fb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("https://www.facebook.com/Cratefreeusa");
            }
        });

        findViewById(R.id.btn_twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("https://twitter.com/CrateFreeUSA");
            }
        });

        findViewById(R.id.btn_instagram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("http://www.instagram.com/cratefreeusa");
            }
        });

        findViewById(R.id.btn_cratefree).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("http://www.cratefreeusa.org/");
            }
        });

        findViewById(R.id.btn_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("https://www.youtube.com/watch?v=WukEcf5i2TQ");
            }
        });

        findViewById(R.id.btn_mail_join).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("http://cratefreeil.us11.list-manage.com/subscribe?u=42c3d81fff809c3be687a1b55&id=e1f93b7278");
            }
        });

        findViewById(R.id.btn_share).setOnClickListener(shareOnClickListener);
        findViewById(R.id.menu_share).setOnClickListener(shareOnClickListener);

        findViewById(R.id.btn_take_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("https://cratefreeusa.org/takeaction/");
            }
        });

        findViewById(R.id.btn_pig_protector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("https://cratefreeusa.org/pigprotectors/");
            }
        });

        findViewById(R.id.btn_volunteer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl("https://cratefreeusa.org/volunteer/");
            }
        });

        findViewById(R.id.btn_disclamer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(AboutActivity.this, R.anim.slide_in_right, R.anim.slide_out_left);
                Intent intent = new Intent(AboutActivity.this, WebActivity.class);
                AboutActivity.this.startActivity(intent, options.toBundle());
            }
        });
    }

    private View.OnClickListener shareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("plain/text");
            sendIntent.putExtra(Intent.EXTRA_TEXT, Constants.SHARE_APP);
            startActivity(Intent.createChooser(sendIntent, String.format(getString(R.string.share_app), getString(R.string.dialog_choose_app))));
        }
    };


    private void openUrl(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);

    }

    private void expand(TextView text, ImageButton button) {

        int duration = 400;

        if (text.getMaxLines() == 5) {
            button.setRotation(180f);

            ObjectAnimator animation = ObjectAnimator.ofInt(text, "maxLines", 30);
            animation.setDuration(duration).start();
        } else {
            button.setRotation(0f);

            ObjectAnimator animation = ObjectAnimator.ofInt(text, "maxLines", 5);
            animation.setDuration(duration).start();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
