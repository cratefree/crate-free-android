package com.creatix.cratefree;

import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebActivity extends BaseActivity {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_web;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        initViews();
    }

    private void initViews() {
        WebView webView = (WebView) findViewById(R.id.webview);

        WebSettings set = webView.getSettings();
        set.setLoadsImagesAutomatically(true);
        set.setLoadWithOverviewMode(true);
        set.setUseWideViewPort(true);
        set.setSupportZoom(false);
        set.setTextZoom(300);
        set.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        set.setBuiltInZoomControls(false);
        set.setJavaScriptEnabled(true);

        set.setDisplayZoomControls(false);
        webView.loadUrl("file:///android_asset/disclaimer.html");

        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
