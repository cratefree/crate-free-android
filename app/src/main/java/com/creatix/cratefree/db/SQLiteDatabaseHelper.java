package com.creatix.cratefree.db;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteOpenHelper;

import com.creatix.cratefree.MyApplication;
import com.creatix.cratefree.utils.Constants;
import com.creatix.cratefree.utils.Utils;

import java.io.IOException;

public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private static final String SQL_DIR = "sql";

    private static final String CREATEFILE = "create.sql";

    private static final String UPGRADEFILE_PREFIX = "upgrade-";

    private static final String UPGRADEFILE_SUFFIX = ".sql";

    private Context context;
    private static SQLiteDatabaseHelper instance;


    public SQLiteDatabaseHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    public static synchronized SQLiteDatabaseHelper getHelper(Context context) throws PackageManager.NameNotFoundException {
        if (instance == null) {
            instance = new SQLiteDatabaseHelper(context, "crateFree.db", null, VersionUtils.getVersionCode(context));
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Utils.appendLog(Constants.TAG_DATABASE, "create database");
            execSqlFile(CREATEFILE, db);
        } catch (IOException exception) {
            throw new RuntimeException("Database creation failed", exception);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            Utils.appendLog(Constants.TAG_DATABASE, "upgrade database from " + oldVersion + " to "
                    + newVersion);
            for (String sqlFile : AssetUtils.list(SQL_DIR, this.context.getAssets())) {
                if (sqlFile.startsWith(UPGRADEFILE_PREFIX)) {
                    int fileVersion = Integer.parseInt(sqlFile.substring(
                            UPGRADEFILE_PREFIX.length(),
                            sqlFile.length() - UPGRADEFILE_SUFFIX.length()));
                    if (fileVersion > oldVersion && fileVersion <= newVersion) {
                        execSqlFile(sqlFile, db);
                    }
                }
                MyApplication.getInstance().requestAppUpdate();
            }
        } catch (IOException exception) {
            throw new RuntimeException("Database upgrade failed", exception);
        }
    }

    protected void execSqlFile(String sqlFile, SQLiteDatabase db) throws SQLException, IOException {

        Utils.appendLog(Constants.TAG_DATABASE, "  exec sql file: " + sqlFile);
        for (String sqlInstruction : SqlParser.parseSqlFile(SQL_DIR + "/" + sqlFile,
                this.context.getAssets())) {
            Utils.appendLog(Constants.TAG_DATABASE, sqlInstruction);
            db.execSQL(sqlInstruction);
        }
    }


    @Override
    public SQLiteDatabase getWritableDatabase() {
        while (true) {
            try {
                return super.getWritableDatabase();
            } catch (SQLiteDatabaseLockedException e) {
                System.err.println(e);
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
    }
}