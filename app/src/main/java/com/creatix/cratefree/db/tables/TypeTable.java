package com.creatix.cratefree.db.tables;

public class TypeTable {

    public static final String TABLE_NAME = "Type";

    public static final String FARM_ID = "farm_id";
    public static final String TYPE = "type";

    public static final String[] allColumns = {TypeTable.FARM_ID, TypeTable.TYPE};

}
