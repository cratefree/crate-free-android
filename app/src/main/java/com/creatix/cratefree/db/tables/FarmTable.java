package com.creatix.cratefree.db.tables;

public class FarmTable {

    public static final String TABLE_NAME = "Farm";

    public static final String ID = "_id";
    public static final String FARM_ID = "farm_id";
    public static final String NAME = "name";
    public static final String BRAND = "brand";
    public static final String V = "__v";
    public static final String URL = "url";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String PERSON = "person";
    public static final String TWITTER = "tw";
    public static final String INSTAGRAM = "ig";
    public static final String FACEBOOK = "fb";
    public static final String LONGITUDE = "lng";
    public static final String LATITUDE = "lat";
    public static final String COUNTY = "county";
    public static final String ZIP = "zip";
    public static final String TOWN = "town";
    public static final String STREET = "street";
    public static final String CATEGORY = "category";
    public static final String PRODUCTS = "products";
    public static final String TYPES = "types";

    public static final String[] allColumns = {FarmTable.ID, FarmTable.FARM_ID, FarmTable.NAME, FarmTable.BRAND,
            FarmTable.V, FarmTable.URL, FarmTable.PHONE, FarmTable.EMAIL, FarmTable.PERSON, FarmTable.LONGITUDE, FarmTable.TWITTER, FarmTable.INSTAGRAM, FarmTable.FACEBOOK,
            FarmTable.LATITUDE, FarmTable.COUNTY, FarmTable.ZIP, FarmTable.TOWN, FarmTable.STREET, FarmTable.CATEGORY, FarmTable.PRODUCTS, FarmTable.TYPES};
}
