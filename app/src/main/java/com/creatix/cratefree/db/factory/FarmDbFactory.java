package com.creatix.cratefree.db.factory;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;

import com.creatix.cratefree.api.objects.Category;
import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.api.objects.Product;
import com.creatix.cratefree.db.tables.FarmTable;
import com.creatix.cratefree.db.tables.ProductTable;
import com.creatix.cratefree.db.tables.TypeTable;
import com.creatix.cratefree.utils.Constants;
import com.creatix.cratefree.utils.JsonUtil;
import com.creatix.cratefree.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static com.creatix.cratefree.utils.Constants.DELIMITER;

public class FarmDbFactory extends BaseDbFactory {

    public FarmDbFactory(Context context) throws NameNotFoundException {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public List<Farm> setFarms(JSONArray jsonArray) {

        List<Farm> farms = new ArrayList<>();

        long start = System.currentTimeMillis();
        database.beginTransaction();
        try {
            List<Long> existingIds = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    Farm farm = setFarm(jsonArray.getJSONObject(i));
                    farms.add(farm);
                    existingIds.add(farm.getId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            deleteUnexistingFarms(existingIds);
        } finally {
            database.setTransactionSuccessful();
            database.endTransaction();
            Utils.appendLog(Constants.TAG_DATABASE, String.valueOf(System.currentTimeMillis() - start));
        }

        return farms;
    }

    /**
     * Create or update existing FarmTable in DB
     *
     * @param json
     * @return FarmTable from DB
     */
    public Farm setFarm(JSONObject json) {

        ContentValues values = getValues(json);

        Farm farm = null;
        try {
            farm = getFarm(json.getString(FarmTable.ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return farm == null ? createFarm(values) : (farm.hashChanged(values) ? updateFarm(farm.getId(), values) : farm);
    }

    /**
     * Create new FarmTable in DB
     *
     * @return refreshed User
     */
    public Farm createFarm(ContentValues values) {

        database.insert(FarmTable.TABLE_NAME, null, values);

        return getFarm(values.getAsString(FarmTable.FARM_ID));
    }

    /**
     * Update FarmTable in DB
     *
     * @return refreshed User
     */
    public Farm updateFarm(Long id, ContentValues values) {

        database.update(FarmTable.TABLE_NAME, values, FarmTable.ID + " = ?", new String[]{id.toString()});

        return getFarm(values.getAsString(FarmTable.FARM_ID));
    }

    private void deleteUnexistingFarms(List<Long> existing) {

        List<Long> dbFarms = getFarmIds();
        Iterator<Long> iterator = dbFarms.iterator();
        while (iterator.hasNext()) {
            Long id = iterator.next();
            if (!existing.contains(id)) {
                deleteFarm(id);
            }
        }
    }

    /**
     * delete FarmTable from DB
     *
     * @param id farm Id
     */
    public void deleteFarm(Long id) {

        Utils.appendLog(Constants.TAG_DATABASE, "Farm deleted: " + id.toString());
        database.delete(FarmTable.TABLE_NAME, FarmTable.ID + " = ?", new String[]{id.toString()});
        database.delete(ProductTable.TABLE_NAME, ProductTable.FARM_ID + " = ?", new String[]{id.toString()});
        database.delete(TypeTable.TABLE_NAME, TypeTable.FARM_ID + " = ?", new String[]{id.toString()});
    }

    /**
     * @return get All farms from DB
     */
    public List<Farm> getAllFarmsWithData() {

        List<Farm> farms = new ArrayList<>();

        long start = System.currentTimeMillis();
        database.beginTransaction();
        Cursor cursor = null;
        try {
            cursor = database.query(FarmTable.TABLE_NAME, FarmTable.allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Farm farm = cursorToFarm(cursor);
                farms.add(farm);
                cursor.moveToNext();
            }
        } finally {
            // Make sure to close the cursor
            cursor.close();
            database.setTransactionSuccessful();
            database.endTransaction();
            Utils.appendLog(Constants.TAG_DATABASE, String.valueOf(System.currentTimeMillis() - start));
        }

        return farms;
    }

    /**
     * @return get Filtered farms
     */
    public List<Farm> getFilteredFarms(List<Product> products, List<Category> categories) {

        List<Farm> farms = new ArrayList<>();

        long start = System.currentTimeMillis();
        database.beginTransaction();
        Cursor cursor = null;

        try {
            cursor = database.rawQuery(buildFilterQuery(products, categories), null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Farm farm = cursorToFarm(cursor);
                farms.add(farm);
                cursor.moveToNext();
            }
        } finally {
            // Make sure to close the cursor
            cursor.close();
            database.setTransactionSuccessful();
            database.endTransaction();
            Utils.appendLog(Constants.TAG_DATABASE, String.valueOf(System.currentTimeMillis() - start));
        }

        return farms;
    }

    private String buildFilterQuery(List<Product> products, List<Category> categories) {

        String query = "SELECT * FROM Farm WHERE ";

        if ((products != null && !products.isEmpty()) && (categories == null || categories.isEmpty())) {
            query += getProductQuery(products);
        } else if ((categories != null && !categories.isEmpty()) && (products == null || products.isEmpty())) {
            query += getCategoryQuery(categories);
        } else {
            query += "(" + getProductQuery(products) + ") AND (" + getCategoryQuery(categories) + ")";
        }

        return query + ";";
    }

    private String getProductQuery(List<Product> products) {

        String query = "";

        for (int i = 0; i < products.size(); i++) {
            if (i == 0) {
                query += "products LIKE '%" + products.get(i).getValue() + "%' ";
            } else {
                query += "OR products LIKE '%" + products.get(i).getValue() + "%' ";
            }
        }

        return query;
    }

    private String getCategoryQuery(List<Category> categories) {

        String query = "";

        for (int i = 0; i < categories.size(); i++) {
            if (i == 0) {
                query += "category LIKE '%" + categories.get(i).getValue() + "%' ";
            } else {
                query += "OR category LIKE '%" + categories.get(i).getValue() + "%' ";
            }
        }

        return query;
    }

    /**
     * @return get All Users from DB
     */
    public List<Long> getFarmIds() {

        List<Long> farmsIds = new ArrayList<>();

        database.beginTransaction();
        Cursor cursor = null;
        try {
            cursor = database.query(FarmTable.TABLE_NAME, new String[]{FarmTable.ID}, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                farmsIds.add(cursor.getLong(cursor.getColumnIndex(FarmTable.ID)));
                cursor.moveToNext();
            }
        } finally {
            // Make sure to close the cursor
            cursor.close();
            database.setTransactionSuccessful();
            database.endTransaction();
        }

        return farmsIds;
    }

    /**
     * @return FarmTable from DB
     */
    public Farm getFarm(Long id) {

        Cursor cursor = null;
        Farm newFarm;

        try {
            cursor = database.query(FarmTable.TABLE_NAME, FarmTable.allColumns, FarmTable.ID + " = ?", new String[]{id.toString()}, null, null, null);
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            newFarm = cursorToFarm(cursor);
        } finally {
            // Make sure to close the cursor
            cursor.close();
        }

        return newFarm;
    }

    /**
     * @return FarmTable from DB
     */
    public Farm getFarm(String farmId) {

        Cursor cursor = null;
        Farm newFarm;

        try {
            cursor = database.query(FarmTable.TABLE_NAME, FarmTable.allColumns, FarmTable.FARM_ID + " = ?", new String[]{farmId}, null, null, null);
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            newFarm = cursorToFarm(cursor);
        } finally {
            // Make sure to close the cursor
            cursor.close();
        }

        return newFarm;
    }

    /**
     * @param cursor
     * @return FarmTable with refreshed data from DB
     */
    public Farm cursorToFarm(Cursor cursor) {

        Farm farm = new Farm();

        farm.setId(cursor.getLong(cursor.getColumnIndex(FarmTable.ID)));
        farm.setFarmId(cursor.getString(cursor.getColumnIndex(FarmTable.FARM_ID)));
        farm.setName(cursor.getString(cursor.getColumnIndex(FarmTable.NAME)));
        farm.setBrand(cursor.getString(cursor.getColumnIndex(FarmTable.BRAND)));
        farm.setV(cursor.getString(cursor.getColumnIndex(FarmTable.V)));
        farm.setURL(cursor.getString(cursor.getColumnIndex(FarmTable.URL)));
        farm.setPhone(cursor.getString(cursor.getColumnIndex(FarmTable.PHONE)));
        farm.setEmail(cursor.getString(cursor.getColumnIndex(FarmTable.EMAIL)));
        farm.setPerson(cursor.getString(cursor.getColumnIndex(FarmTable.PERSON)));
        farm.setTwitter(cursor.getString(cursor.getColumnIndex(FarmTable.TWITTER)));
        farm.setInstagram(cursor.getString(cursor.getColumnIndex(FarmTable.INSTAGRAM)));
        farm.setFacebook(cursor.getString(cursor.getColumnIndex(FarmTable.FACEBOOK)));
        farm.setLongitude(cursor.getString(cursor.getColumnIndex(FarmTable.LONGITUDE)));
        farm.setLatitude(cursor.getString(cursor.getColumnIndex(FarmTable.LATITUDE)));
        farm.setCounty(cursor.getString(cursor.getColumnIndex(FarmTable.COUNTY)));
        farm.setZip(cursor.getString(cursor.getColumnIndex(FarmTable.ZIP)));
        farm.setTown(cursor.getString(cursor.getColumnIndex(FarmTable.TOWN)));
        farm.setStreet(cursor.getString(cursor.getColumnIndex(FarmTable.STREET)));
        farm.setCategory(Category.findByCategory(cursor.getString(cursor.getColumnIndex(FarmTable.CATEGORY))));

        String productsString = cursor.getString(cursor.getColumnIndex(FarmTable.PRODUCTS));
        farm.setProducts(Arrays.asList((null == productsString ? "" : productsString).split(DELIMITER)));

        String typesString = cursor.getString(cursor.getColumnIndex(FarmTable.TYPES));
        farm.setTypes(Arrays.asList((null == typesString ? "" : typesString).split(DELIMITER)));

        return farm;
    }

    /**
     * @return Values to be written into DB
     */
    private ContentValues getValues(JSONObject json) {

        ContentValues values = new ContentValues();

        try {
            values.put(FarmTable.FARM_ID, JsonUtil.optString(json, FarmTable.ID));
            values.put(FarmTable.NAME, JsonUtil.optString(json, FarmTable.NAME));
            values.put(FarmTable.BRAND, JsonUtil.optString(json, FarmTable.BRAND));
            values.put(FarmTable.V, JsonUtil.optString(json, FarmTable.V));

            if (json.has("category")) {
                values.put(FarmTable.CATEGORY, JsonUtil.optString(json, FarmTable.CATEGORY));
            }

            if (json.has("contact")) {
                JSONObject contact = json.getJSONObject("contact");
                values.put(FarmTable.URL, JsonUtil.optString(contact, FarmTable.URL));
                values.put(FarmTable.PHONE, JsonUtil.optString(contact, FarmTable.PHONE));
                values.put(FarmTable.EMAIL, JsonUtil.optString(contact, FarmTable.EMAIL));
                values.put(FarmTable.PERSON, JsonUtil.optString(contact, FarmTable.PERSON));
                values.put(FarmTable.TWITTER, JsonUtil.optString(contact, FarmTable.TWITTER));
                values.put(FarmTable.INSTAGRAM, JsonUtil.optString(contact, FarmTable.INSTAGRAM));
                values.put(FarmTable.FACEBOOK, JsonUtil.optString(contact, FarmTable.FACEBOOK));
            }

            if (json.has("location")) {
                JSONObject location = json.getJSONObject("location");
                values.put(FarmTable.LONGITUDE, JsonUtil.optString(location, FarmTable.LONGITUDE));
                values.put(FarmTable.LATITUDE, JsonUtil.optString(location, FarmTable.LATITUDE));
            }

            if (json.has("address")) {
                JSONObject address = json.getJSONObject("address");
                values.put(FarmTable.COUNTY, JsonUtil.optString(address, FarmTable.COUNTY));
                values.put(FarmTable.ZIP, JsonUtil.optString(address, FarmTable.ZIP));
                values.put(FarmTable.TOWN, JsonUtil.optString(address, FarmTable.TOWN));
                values.put(FarmTable.STREET, JsonUtil.optString(address, FarmTable.STREET));
            }

            if (json.has("products")) {
                JSONArray productsArray = json.getJSONArray("products");
                values.put(FarmTable.PRODUCTS, getStringArray(productsArray));
            }
            if (json.has("types")) {
                JSONArray typesArray = json.getJSONArray("types");
                values.put(FarmTable.TYPES, getStringArray(typesArray));
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return values;
    }

    private String getStringArray(JSONArray jsonArray) {
        String stringArray = "";

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                stringArray += getFarmType(jsonArray.getString(i));
                if (i < jsonArray.length() - 1) {
                    stringArray += DELIMITER;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return stringArray;
    }

    private String getFarmType(String rawString) {
        switch (rawString) {
            case "winter-farmers-market":
                return "Winter Farmer's Market";
            case "farm-store":
                return "Farm Store";
            case "farm-tours":
                return "Farm Tours";
            default:
                return rawString;
        }
    }
}