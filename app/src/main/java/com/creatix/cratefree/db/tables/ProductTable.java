package com.creatix.cratefree.db.tables;

public class ProductTable {

    public static final String TABLE_NAME = "Product";

    public static final String FARM_ID = "farm_id";
    public static final String PRODUCT = "product";

    public static final String[] allColumns = {ProductTable.FARM_ID, ProductTable.PRODUCT};

}
