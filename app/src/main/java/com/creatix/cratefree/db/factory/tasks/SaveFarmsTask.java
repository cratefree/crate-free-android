package com.creatix.cratefree.db.factory.tasks;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import com.creatix.cratefree.api.objects.Farm;
import com.creatix.cratefree.db.factory.FarmDbFactory;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by jano on 26.11.2015.
 */
public class SaveFarmsTask extends AsyncTask<Void, Void, ArrayList<Farm>> {

    private JSONArray farmsJsonArray;
    private FarmDbFactory farmDbFactory;

    public SaveFarmsTask(Activity activity, JSONArray jsonArray) {
        this.farmsJsonArray = jsonArray;
        try {
            farmDbFactory = new FarmDbFactory(activity);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected ArrayList<Farm> doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Farm> farms) {
        super.onPostExecute(farms);
    }
}
