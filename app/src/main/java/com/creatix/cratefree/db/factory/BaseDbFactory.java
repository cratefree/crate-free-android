package com.creatix.cratefree.db.factory;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.creatix.cratefree.db.SQLiteDatabaseHelper;

public abstract class BaseDbFactory {

    // Database fields
    protected SQLiteDatabase database;
    protected SQLiteDatabaseHelper dbHelper;

    protected Context context;

    public BaseDbFactory(Context context) throws NameNotFoundException {

        dbHelper = SQLiteDatabaseHelper.getHelper(context);

        this.context = context;
    }

    public BaseDbFactory(SQLiteDatabase database) {
        // TODO Auto-generated constructor stub
        this.database = database;

    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
//        dbHelper.close();
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }

}