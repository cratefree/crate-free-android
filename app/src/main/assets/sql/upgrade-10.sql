DROP TABLE IF EXISTS Farm;
DROP TABLE IF EXISTS Product;
DROP TABLE IF EXISTS Type;

CREATE TABLE Farm (
	_id INTEGER PRIMARY KEY AUTOINCREMENT,
	farm_id TEXT,
	name TEXT,
	brand TEXT,
	__v TEXT,
	url TEXT,
	phone TEXT,
	email TEXT,
	person TEXT,
	tw TEXT,
	ig TEXT,
	fb TEXT,
	lng TEXT,
	lat TEXT,
	county TEXT,
	zip TEXT,
	town TEXT,
	street TEXT,
	products TEXT,
	types TEXT
);

CREATE TABLE Product (
	farm_id TEXT,
	product TEXT,
	FOREIGN KEY(farm_id) REFERENCES Farm(_id)
);

CREATE TABLE Type (
	farm_id TEXT,
	type TEXT,
    FOREIGN KEY(farm_id) REFERENCES Farm(_id)
);

CREATE INDEX product_farm_id ON Product(farm_id);
CREATE INDEX type_farm_id ON Product(farm_id);
CREATE INDEX farm_id ON Farm(farm_id);